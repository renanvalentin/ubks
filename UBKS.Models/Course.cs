﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UBKS.Models
{
    public class Course : Entity
    {
        public string Name { get; set; }

        public virtual Unit Unit { get; set; }
        public virtual ICollection<Class> Classes { get; set; }
    }
}
