﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UBKS.Models
{
    public class StudentContact : Contact
    {
        public virtual Student Student { get; set; }
    }
}
