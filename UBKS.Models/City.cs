﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UBKS.Models
{
    public class City : Entity
    {
        public string Name { get; set; }
        
        public virtual State State { get; set; }
        public virtual ICollection<Unit> Units { get; set; }
    }
}
