﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBKS.Models
{
    public class Enrollment : Entity
    {
        public virtual DateTime InscriptionDate { get; set; }
        public virtual bool Scholarship { get; set; }

        public virtual Student Student { get; set; }
        public virtual Class Class { get; set; }
    }
}
