﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UBKS.Models
{
    public class Presence : Entity
    {
        //public long AlunoID { get; set; }

        public DateTime PresenceDate { get; set; }
        public bool IsPresent { get; set; }

        public virtual Student Student { get; set; }
    }
}
