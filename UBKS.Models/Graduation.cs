﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UBKS.Models
{
    public class Graduation : Entity
    {
        public string Degree { get; set; }
        public string StripColor { get; set; }

        public virtual ICollection<StudentGraduation> StudentGraduation { get; set; }
    }
}
