﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UBKS.Models
{
    public class Payment : Entity
    {
        public DateTime PaymentDate { get; set; }

        public virtual Student Student { get; set; }
    }
}
