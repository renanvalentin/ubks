﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UBKS.Models
{
    public class State : Entity
    {
        public string Name { get; set; }
        public string Initials { get; set; }

        public virtual ICollection<City> Cities { get; set; }
    }
}
