﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UBKS.Models
{
    public class Education : Entity
    {
        public virtual string Institution { get; set; }
        public virtual string Course { get; set; }
        public virtual string Period { get; set; }
        public virtual string Grade { get; set; }
        public virtual DateTime? BeginDate { get; set; }
        public virtual DateTime? EndDate { get; set; }

        public virtual Student Student { get; set; }
    }
}
