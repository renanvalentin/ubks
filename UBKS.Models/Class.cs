﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UBKS.Models
{
    public class Class : Entity
    {
        public string Nome { get; set; }

        public string DaysOfWeek { get; set; }

        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }

        public DateTime BeginTime { get; set; }
        public DateTime EndTime { get; set; }

        public virtual Course Course { get; set; }
        public virtual ICollection<Enrollment> Enrollments { get; set; }
    }
}
