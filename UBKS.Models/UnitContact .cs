﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UBKS.Models
{
    public class UnitContact : Contact
    {
        public virtual Unit Unit { get; set; }
    }
}
