﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UBKS.Models
{
    public class Unit : Entity
    {
        public string Name { get; set; }

        public virtual Organization Organization { get; set; }
        public virtual City City { get; set; }
        public virtual Address Address { get; set; }
        
        public virtual ICollection<Student> Students { get; set; }
        public virtual ICollection<Course> Courses { get; set; }
        public virtual ICollection<UnitContact> Contacts { get; set; }
    }
}
