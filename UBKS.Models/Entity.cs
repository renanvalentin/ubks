﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UBKS.Models
{
    public abstract class Entity
    {
        public virtual long Id { get; set; }
    }
}
