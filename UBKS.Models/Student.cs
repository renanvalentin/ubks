﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UBKS.Models
{
    public class Student : Person, IValidatableObject
    {
        public Student()
        {
            this.Contacts = new List<StudentContact>();
            this.Educations = new List<Education>();
        }

        public string CodeTKFI { get; set; }

        public virtual Responsible Responsible { get; set; }

        public virtual Unit Unit { get; set; }

        public virtual Address Address { get; set; }

        public virtual ICollection<Payment> Payments { get; set; }

        public virtual ICollection<StudentContact> Contacts { get; set; }

        public virtual ICollection<Presence> Presences { get; set; }

        public virtual ICollection<StudentGraduation> StudentGraduations { get; set; }

        public virtual ICollection<Education> Educations { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (CalculateAge(this.BirthDate) < 18 && this.Responsible == null)
                yield return new ValidationResult("Estudantes menores de 18 anos precisam dos dados do responsável", new[] { "BirthDate" });
        }

        internal int CalculateAge(DateTime birthDate)
        {
            var now = DateTime.Now;
            var age = now.Year - birthDate.Year;
            if (now.Month < birthDate.Month || (now.Month == birthDate.Month && now.Day < birthDate.Day)) age--;
            return age;
        }
    }
}
