﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UBKS.Models
{
    public class StudentGraduation : Entity
    {
        public DateTime ExamDate { get; set; }
        public string NumberDegreeIssued { get; set; }

        public virtual Student Student { get; set; }
        public virtual Graduation Graduation { get; set; }
    }
}
