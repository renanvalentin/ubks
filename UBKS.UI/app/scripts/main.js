/*global require*/
'use strict';

require.config({
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        },
        stickit: {
            deps: [
                'backbone'
            ]
        },
        moment: {
            deps: [
                'jquery'
            ],
            exports: 'moment'
        }
    },
    paths: {
        jquery: '../bower_components/jquery/jquery',
        backbone: '../bower_components/backbone/backbone',
        underscore: '../bower_components/underscore/underscore',
        validation: '../bower_components/backbone-validation/dist/backbone-validation-amd',
        stickit: '../bower_components/backbone.stickit/backbone.stickit',
        moment: '../bower_components/momentjs/moment'
    }
});

require([
    'backbone',
    'router',
    'app',
    'stickit'
], function (Backbone, App) {
    App.initialize();    
});
