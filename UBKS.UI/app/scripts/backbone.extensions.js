﻿/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'validation'
], function ($, _, Backbone, Validation) {
    'use strict';

    var validation = function () {
        Backbone.Validation.configure({
            forceUpdate: true
        });

        _.extend(Backbone.Validation.callbacks, {
            valid: function (view, attr, selector) {
                view.$('[name=' + attr + ']').removeClass('invalid');
            },
            invalid: function (view, attr, error, selector) {
                view.$('[name=' + attr + ']').addClass('invalid');
            }
        });

        _.extend(Backbone.Validation.patterns, {
            data: /(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}/
        });

        _.extend(Backbone.Validation.messages, {
            data: 'Campo data inválido'
        });
    },

    close = function () {
        Backbone.View.prototype.close = function () {
            if (this.onClose) {
                this.onClose();
            }

            this.remove();
            this.unbind();
        };
    },

    register = function () {
        validation();
        close();
    };

    return {
        register: register
    };
});