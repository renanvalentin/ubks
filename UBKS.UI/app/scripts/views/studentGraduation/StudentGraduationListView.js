/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'models/studentGraduation',
    'collections/graduation'
], function ($, _, Backbone, JST, StudentGraduation, GraduationCollection) {
    'use strict';

    var StudentGraduationView = Backbone.View.extend({
        tagName: 'tr',

        template: JST['app/scripts/templates/studentGraduation/StudentGraduationListView.ejs'],

        initialize: function () {
            if (!this.model)
                throw new Error('Student Graduation model cannot be null!!');
        },

        render: function () {
            this.$el.html(this.template(this.model.toJSON()));
          
            return this;
        }
    });

    return StudentGraduationView;
});