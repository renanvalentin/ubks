/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'models/studentGraduation',
    'collections/graduation',
    'collections/studentGraduation',
    'views/studentGraduation/studentGraduationListView'
], function ($, _, Backbone, JST, StudentGraduation, GraduationCollection, StudentGraduationCollection, StudentGraduationListView) {
    'use strict';

    var StudentGraduationView = Backbone.View.extend({
        tagName: 'section',
        className: 'page-sidebar',

        template: JST['app/scripts/templates/studentGraduation/StudentGraduationView.ejs'],

        studentId: 0,

        initialize: function () {
            if (!this.model)
                throw new Error('Student Graduation model cannot be null!!');

            this.studentId = this.model.get('studentId');

            this.collection = new StudentGraduationCollection();

            this.listenTo(this.collection, 'reset', this.renderStudentGraduation);

            this.collection.fetch({ reset: true, data: { id: this.studentId } });

            this.fetchGraduationCollection();

            Backbone.Validation.bind(this);
        },

        events: {
            'click #addGraduation': 'addGraduation',
            'click #cancelGraduation': 'cancelGraduation'
        },

        bindings: {
            '[name=graduationId]': {
                observe: 'graduationId',
                selectOptions: {
                    collection: function () {
                        var items = this.graduationCollectino.toJSON();

                        return items;
                    },
                    defaultOption: {
                        label: 'Selecione...',
                        value: null
                    },
                    labelPath: 'degree',
                    valuePath: 'id'
                }
            },
            '[name=examDate]': {
                observe: 'examDate',
                setOptions: {
                    validate: true
                }
            }
        },

        render: function () {
            this.$el.html(this.template);

            return this;
        },

        renderStudentGraduation: function () {
            var $el = this.$el;

            $el.find('[data-graduation]').empty();

            this.collection.each(function (studentGraduation) {
                var itemView = new StudentGraduationListView({ model: studentGraduation });
                $el.find('[data-graduation]').append(itemView.render().el);
            });
        },

        fetchGraduationCollection: function () {
            this.graduationCollectino = new GraduationCollection();
            this.listenTo(this.graduationCollectino, 'reset', this.render());

            var _this = this;

            this.graduationCollectino.fetch().done(function (data) {
                _this.unstickit();
                _this.stickit();
            });
        },

        addGraduation: function (e) {
            e.preventDefault();

            if (!this.model.isValid(true))
                return;

            var _this = this;

            this.model.set('studentId', this.studentId);

            this.model.save(this.model.toJSON(), {
                success: function (data) {
                    _this.reset();
                    _this.collection.fetch({ reset: true, data: { id: _this.studentId } });
                }
            });
        },

        cancelGraduation: function (e) {
            e.preventDefault();

            Backbone.Validation.unbind(this);
            this.unstickit();

            this.trigger('remove');
        },

        reset: function () {
            this.model.clear();
        }
    });

    return StudentGraduationView;
});