/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'models/student',
    'views/address/addressView',
    'views/education/educationView',
    'views/contact/contact',
    'views/responsible/responsibleView',
    'views/studentGraduation/studentGraduationView',
], function ($, _, Backbone, JST, Student, AddressView, EducationView, ContactView, ResponsibleView, StudentGraduationView) {
    'use strict';

    var StudentView = Backbone.View.extend({
        tagName: 'section',
        className: 'page-container',

        template: JST['app/scripts/templates/student/student.ejs'],

        initialize: function () {
            if (!this.model)
                this.model = new Student();

            this.addressView = new AddressView();
            this.educationView = new EducationView();
            this.contactView = new ContactView();

            this.listenTo(this.model, 'change:birthDate', this.ageValidation);

            Backbone.Validation.bind(this);
        },

        events: {
            'click #save': 'save'
        },

        bindings: {
            '[name=codeTKFI]': 'codeTKFI',
            '[name=firstName]': {
                observe: 'firstName',
                setOptions: {
                    validate: true
                }
            },
            '[name=lastName]': {
                observe: 'lastName',
                setOptions: {
                    validate: true
                }
            },
            '[name=birthDate]': {
                observe: 'birthDate',
                setOptions: {
                    validate: true
                }
            },
            '[name=rg]': {
                observe: 'rg',
                setOptions: {
                    validate: true
                }
            },
            '[name=cpf]': {
                observe: 'cpf',
                setOptions: {
                    validate: true
                }
            }
        },

        render: function () {
            this.$el.html(this.template(this.model.toJSON()));
            this.stickit();

            this.renderAddressView();
            this.renderEducationView();
            this.renderContactView();

            return this;
        },

        renderAddressView: function () {
            var address = this.addressView.render().el;
            this.$('.page-form').append(address);
        },

        renderEducationView: function () {
            var education = this.educationView.render().el;
            this.$('.page-form').append(education);
        },

        renderContactView: function () {
            var education = this.contactView.render().el;
            this.$('.page-form').append(education);
        },

        renderResponsible: function () {
            if (this.responsibleView)
                return false;

            this.responsibleView = new ResponsibleView();
            var view = this.responsibleView.render().el;

            this.$('.page-form article[data-id=studentDocument]').after(view);
        },

        removeResponsibleView: function () {
            if (this.responsibleView) {
                this.responsibleView.remove();
            }
        },

        ageValidation: function (e) {
            var currentAge = this.model.isValid('birthDate') && moment(this.model.get('birthDate'), 'DD-MM-YYYY');

            if (currentAge && currentAge.isValid()) {
                var age = parseInt(currentAge.fromNow().replace('years ago'));

                if (age < 18) {
                    this.renderResponsible();
                } else {
                    this.removeResponsibleView();
                }
            }

            return false;
        },

        save: function (e) {
            e.preventDefault();

            if (!this.validate())
                return false;

            this.model.set('address', this.addressView.model.toJSON());
            this.model.set('educations', [this.educationView.model.toJSON()]);
            this.model.set('contacts', [this.contactView.model.toJSON()]);

            if (this.responsibleView)
                this.model.set('responsible', this.responsibleView.model.toJSON());

            this.model.set('unit', 1);

            this.model.save(this.model.toJSON(), {
                success: function (data) {
                    Backbone.history.navigate('student/edit/' + data.id, true);
                }
            });
        },

        validate: function () {
            if (!this.model.isValid(true) ||
                !this.addressView.model.isValid(true) ||
                !this.educationView.model.isValid(true) ||
                !this.contactView.model.isValid(true))
                return false;

            return true;
        },

        onClose: function () {
            Backbone.Validation.unbind(this);
            this.unstickit();
        }
    });

    return StudentView;
});