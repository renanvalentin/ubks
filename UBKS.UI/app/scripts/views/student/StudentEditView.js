/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'models/student',
    'models/address',
    'models/contact',
    'models/education',
    'models/studentGraduation',
    'models/payment',
    'views/address/addressView',
    'views/education/educationView',
    'views/contact/contact',
    'views/responsible/responsibleView',
    'views/studentGraduation/StudentGraduationView',
    'views/payment/PaymentView'
], function ($, _, Backbone, JST, Student, Address, Contact, Education, StudentGraduation, Payment, AddressView, EducationView, ContactView, ResponsibleView, StudentGraduationView, PaymentView) {
    'use strict';

    var StudentView = Backbone.View.extend({
        tagName: 'section',
        className: 'page-container',

        template: JST['app/scripts/templates/student/StudentEditView.ejs'],

        initialize: function () {
            if (!this.model)
                throw new Error('Student model cannot be null!!');

            var student = this.model.toJSON();

            this.addressView = new AddressView({ model: new Address(student.address) });
            this.educationView = new EducationView({ model: new Education(student.educations && student.educations[0]) });
            this.contactView = new ContactView({ model: new Contact(student.contacts && student.contacts[0]) });

            this.listenTo(this.model, 'change:birthDate', this.ageValidation);

            Backbone.Validation.bind(this);
        },

        events: {
            'click #save': 'save',
            'click #graduation': 'renderGraduation',
            'click #payment': 'renderPayment',
            'blur #birthDate': 'ageValidation'
        },

        bindings: {
            '[name=codeTKFI]': 'codeTKFI',
            '[name=firstName]': {
                observe: 'firstName',
                setOptions: {
                    validate: true
                }
            },
            '[name=lastName]': {
                observe: 'lastName',
                setOptions: {
                    validate: true
                }
            },
            '[name=birthDate]': {
                observe: 'birthDate',
                setOptions: {
                    validate: true
                }
            },
            '[name=rg]': {
                observe: 'rg',
                setOptions: {
                    validate: true
                }
            },
            '[name=cpf]': {
                observe: 'cpf',
                setOptions: {
                    validate: true
                }
            }
        },

        render: function () {
            this.$el.html(this.template(this.model.toJSON()));
            this.stickit();

            this.renderAddressView();
            this.renderEducationView();
            this.renderContactView();

            this.ageValidation();

            return this;
        },

        renderAddressView: function () {
            var address = this.addressView.render().el;
            this.$('.page-form').append(address);
        },

        renderEducationView: function () {
            var education = this.educationView.render().el;
            this.$('.page-form').append(education);
        },

        renderContactView: function () {
            var education = this.contactView.render().el;
            this.$('.page-form').append(education);
        },

        renderResponsible: function () {
            if (this.responsibleView)
                return false;

            this.responsibleView = new ResponsibleView();
            var view = this.responsibleView.render().el;

            this.$('.page-form article[data-id=studentDocument]').after(view);
        },

        renderGraduation: function (e) {
            e.preventDefault();

            if (this.graduationView)
                return false;

            var studentId = this.model.get('id');

            this.graduationView = new StudentGraduationView({ model: new StudentGraduation({ studentId: studentId }) });

            this.listenTo(this.graduationView, 'remove', this.removeGraduation);

            var render = this.graduationView.render().el;

            this.$el.before(render);
        },

        removeGraduation: function () {
            this.graduationView.remove();

            delete this.graduationView;
        },

        renderPayment: function (e) {
            e.preventDefault();

            if (this.paymentView)
                return false;

            var studentId = this.model.get('id');

            this.paymentView = new PaymentView({ model: new Payment({ studentId: studentId }) });

            this.listenTo(this.paymentView, 'remove', this.removePayment);

            var render = this.paymentView.render().el;

            this.$el.before(render);
        },

        removePayment: function () {
            this.paymentView.remove();

            delete this.paymentView;
        },

        removeResponsibleView: function () {
            if (this.responsibleView) {
                this.responsibleView.remove();
            }
        },

        ageValidation: function () {
            var currentAge = this.model.isValid('birthDate') && moment(this.model.get('birthDate'), 'DD-MM-YYYY');

            if (currentAge && currentAge.isValid()) {
                var age = parseInt(currentAge.fromNow().replace('years ago'));

                if (age < 18) {
                    this.renderResponsible();
                } else {
                    this.removeResponsibleView();
                }
            }

            return false;
        },

        save: function (e) {
            e.preventDefault();

            if (!this.validate())
                return false;

            this.model.set('address', this.addressView.model.toJSON());
            this.model.set('educations', [this.educationView.model.toJSON()]);
            this.model.set('contacts', [this.contactView.model.toJSON()]);

            if (this.responsibleView)
                this.model.set('responsible', this.responsibleView.model.toJSON());

            this.model.set('unit', 1);

            this.model.save(this.model.toJSON(), {
                success: function (data) {
                    Backbone.history.navigate('student/edit/' + data.id, true);
                }
            });
        },

        validate: function () {
            if (!this.model.isValid(true) ||
                !this.addressView.model.isValid(true) ||
                !this.educationView.model.isValid(true) ||
                !this.contactView.model.isValid(true))
                return false;

            return true;
        },

        onClose: function () {
            Backbone.Validation.unbind(this);
            this.unstickit();
        }
    });

    return StudentView;
});