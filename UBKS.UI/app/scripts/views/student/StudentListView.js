/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'views/student/studentListItemView'
], function ($, _, Backbone, JST, StudentListItemView) {
    'use strict';

    var StudentListView = Backbone.View.extend({
        tagName: 'section',
        className: 'page-container',

        template: JST['app/scripts/templates/student/StudentListView.ejs'],

        initialize: function() {

        },

        render: function () {
            var $el = this.$el;

            $el.html(this.template);

            this.collection.each(function (student) {
                var itemView = new StudentListItemView({ model: student });
                $el.find('.page-content').append(itemView.render().el);
            });

            return this;
        }
    });

    return StudentListView;
});