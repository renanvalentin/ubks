/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'models/education'
], function ($, _, Backbone, JST, EducationModel) {
    'use strict';

    var EducationView = Backbone.View.extend({
        template: JST['app/scripts/templates/education/education.ejs'],

        initialize: function () {
            if (!this.model)
                this.model = new EducationModel();

            Backbone.Validation.bind(this);
        },

        bindings: {
            '[name=institution]': {
                observe: 'institution',
                setOptions: {
                    validate: true
                }
            },
            '[name=course]': {
                observe: 'course',
                setOptions: {
                    validate: true
                }
            },
            '[name=city]': {
                observe: 'city',
                setOptions: {
                    validate: true
                }
            },
            '[name=period]': {
                observe: 'period',
                setOptions: {
                    validate: true
                }
            },
            '[name=grade]': {
                observe: 'grade',
                setOptions: {
                    validate: true
                }
            },
            '[name=beginDate]': {
                observe: 'beginDate',
                setOptions: {
                    validate: true
                }
            }
        },

        render: function () {
            this.$el.html(this.template(this.model.toJSON()));
            this.stickit();

            return this;
        }
    });

    return EducationView;
});