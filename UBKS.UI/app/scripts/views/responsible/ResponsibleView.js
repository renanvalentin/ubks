/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'models/student'
], function ($, _, Backbone, JST, ResponsibleModel) {
    'use strict';

    var ResponsibleView = Backbone.View.extend({
        template: JST['app/scripts/templates/responsible/ResponsibleView.ejs'],


        initialize: function () {
            if (!this.model)
                this.model = new ResponsibleModel();

            Backbone.Validation.bind(this);
        },

        bindings: {
            '[name=firstName]': {
                observe: 'firstName',
                setOptions: {
                    validate: true
                }
            },
            '[name=lastName]': {
                observe: 'lastName',
                setOptions: {
                    validate: true
                }
            },
            '[name=birthDate]': {
                observe: 'birthDate',
                setOptions: {
                    validate: true
                }
            },
            '[name=rg]': {
                observe: 'rg',
                setOptions: {
                    validate: true
                }
            },
            '[name=cpf]': {
                observe: 'cpf',
                setOptions: {
                    validate: true
                }
            }
        },

        render: function () {
            this.$el.html(this.template(this.model.toJSON()));
            this.stickit();

            return this;
        },

        onClose: function () {
            Backbone.Validation.unbind(this);
            this.unstickit();
        }
    });

    return ResponsibleView;
});