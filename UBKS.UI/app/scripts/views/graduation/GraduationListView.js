/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'views/graduation/graduationListItemView'
], function ($, _, Backbone, JST, GraduationListItemView) {
    'use strict';

    var GraduationListView = Backbone.View.extend({
        tagName: 'section',
        className: 'page-container',

        template: JST['app/scripts/templates/graduation/GraduationListView.ejs'],

        render: function () {
            var $el = this.$el;

            $el.html(this.template);

            this.collection.each(function (student) {
                var itemView = new GraduationListItemView({ model: student });
                $el.find('.page-content').append(itemView.render().el);
            });

            return this;
        }
    });

    return GraduationListView;
});