/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'models/graduation'
], function ($, _, Backbone, JST, Graduation) {
    'use strict';

    var GraduationView = Backbone.View.extend({
        tagName: 'section',
        className: 'page-container',

        template: JST['app/scripts/templates/graduation/GraduationView.ejs'],

        initialize: function () {
            if (!this.model)
                this.model = new Graduation();

            Backbone.Validation.bind(this);
        },

        events: {
            'click #save': 'save'
        },

        bindings: {
            '[name=degree]': {
                observe: 'degree',
                setOptions: {
                    validate: true
                }
            },
            '[name=stripColor]': {
                observe: 'stripColor',
                setOptions: {
                    validate: true
                }
            }
        },

        render: function () {
            this.$el.html(this.template(this.model.toJSON()));
            this.stickit();

            return this;
        },

        save: function (e) {
            e.preventDefault();

            if (!this.model.isValid(true))
                return false;

            this.model.save(this.model.toJSON(), {
                success: function (data) {
                    Backbone.history.navigate('graduation/edit/' + data.id, true);
                }
            });
        },

        onClose: function () {
            Backbone.Validation.unbind(this);
            this.unstickit();
        }
    });

    return GraduationView;
});