/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates'
], function ($, _, Backbone, JST) {
    'use strict';

    var GraduationListItemView = Backbone.View.extend({
        tagName: 'article',

        className: 'page-list-item',

        template: JST['app/scripts/templates/graduation/GraduationListItemView.ejs'],

        render: function () {
            this.$el.html(this.template(this.model.toJSON()));

            return this;
        }
    });

    return GraduationListItemView;
});