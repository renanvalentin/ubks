/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'models/address'
], function ($, _, Backbone, JST, Address) {
    'use strict';

    var AddressView = Backbone.View.extend({
        template: JST['app/scripts/templates/address/address.ejs'],

        initialize: function () {
            if (!this.model)
                this.model = new Address();

            Backbone.Validation.bind(this);
        },

        events: {
            'change #state': 'setState'
        },

        bindings: {
            '[name=zipCode]': {
                observe: 'zipCode',
                setOptions: {
                    validate: true
                }
            },
            '[name=state]': {
                observe: 'state',
                setOptions: {
                    validate: true
                }
            },
            '[name=city]': {
                observe: 'city',
                setOptions: {
                    validate: true
                }
            },
            '[name=neighborhood]': {
                observe: 'neighborhood',
                setOptions: {
                    validate: true
                }
            },
            '[name=street]': {
                observe: 'street',
                setOptions: {
                    validate: true
                }
            },
            '[name=number]': {
                observe: 'number',
                setOptions: {
                    validate: true
                }
            }
        },

        setState: function () {
            var state = this.$('#state').val();
            this.model.set('state', state);
        },

        render: function () {
            this.$el.html(this.template(this.model.toJSON()));
            this.stickit();

            return this;
        }
    });

    return AddressView;
});