/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'models/contact'
], function ($, _, Backbone, JST, Contact) {
    'use strict';

    var ContactView = Backbone.View.extend({
        template: JST['app/scripts/templates/contact/contact.ejs'],

        initialize: function () {
            if (!this.model)
                this.model = new Contact();

            Backbone.Validation.bind(this);
        },

        bindings: {
            '[name=name]': {
                observe: 'name',
                setOptions: {
                    validate: true
                }
            },
            '[name=cellPhone]': {
                observe: 'cellPhone',
                setOptions: {
                    validate: true
                }
            },
            '[name=phone]': {
                observe: 'phone',
                setOptions: {
                    validate: true
                }
            },
            '[name=email]': {
                observe: 'email',
                setOptions: {
                    validate: true
                }
            }
        },

        render: function () {
            this.$el.html(this.template(this.model.toJSON()));
            this.stickit();

            return this;
        }
    });

    return ContactView;
});