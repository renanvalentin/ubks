﻿/*global define*/

define([
    'jquery',
    'underscore',
    'backbone.extensions'
], function ($, _, BackboneExtensions) {
    'use strict';

    BackboneExtensions.register();

    var api = 'http://localhost:2629'

    return {
        api: api
    };
});