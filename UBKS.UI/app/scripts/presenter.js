﻿/*global define*/

define([
    'jquery'
], function ($) {
    'use strict';

    $(document).ajaxStart(function () {
        $('.busy-loading').toggle(true);
    }).ajaxStop(function () {
        $('.busy-loading').toggle(false);
    });

    var switchPage = function (title, view) {
        if (this.currentView) {
            this.currentView.close();
        }

        this.currentView = view;

        $('.application-views').empty();
        $('.application-views').append(view.render().el);

        $('h1[data-bind="title"]').text(title);

        window.getComputedStyle(view.$el[0]).getPropertyValue("left");
        view.$el.toggleClass('active');
    };

    return {
        switchPage: switchPage
    };
});