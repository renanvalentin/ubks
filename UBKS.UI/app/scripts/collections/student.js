/*global define*/

define([
    'underscore',
    'backbone',
    'models/student'
], function (_, Backbone, StudentModel) {
    'use strict';

    var StudentCollection = Backbone.Collection.extend({
        url: '/api/student/',

        model: StudentModel
    });

    return StudentCollection;
});