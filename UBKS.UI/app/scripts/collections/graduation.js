/*global define*/

define([
    'underscore',
    'backbone',
    'models/graduation'
], function (_, Backbone, GraduationModel) {
    'use strict';

    var GraduationCollection = Backbone.Collection.extend({
        url: '/api/graduation/',

        model: GraduationModel
    });

    return GraduationCollection;
});