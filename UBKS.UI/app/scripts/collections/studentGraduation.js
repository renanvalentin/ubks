/*global define*/

define([
    'underscore',
    'backbone',
    'models/studentGraduation'
], function (_, Backbone, StudentGraduationModel) {
    'use strict';

    var StudentGraduationCollection = Backbone.Collection.extend({
        url: '/api/studentGraduation/',

        model: StudentGraduationModel
    });

    return StudentGraduationCollection;
});