/*global define*/

define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    'use strict';

    var GraduationModel = Backbone.Model.extend({
        url: '/api/graduation/',

        defaults: {
            degree: '',
            stripColor: ''
        },

        validation: {
            degree: {
                required: true
            },
            stripColor: {
                required: true
            }
        }
    });

    return GraduationModel;
});