/*global define*/

define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    'use strict';

    var ContactContactModel = Backbone.Model.extend({
        defaults: {
            name: '',
            cellPhone: '',
            phone: '',
            email: ''
        },

		validation: {
			name: {
			  required: true,
			  maxLength: 50
			},
			cellPhone: {
              required: false,
			  maxLength: 15
			},
			phone: {
			  required: true,
			  maxLength: 15
			},
			email: {
              required: false,
			  pattern: 'email',
			  maxLength: 30
			}			
		}           
    });

    return ContactContactModel;
});