/*global define*/

define([
    'underscore',
    'backbone',
    'validation'
], function (_, Backbone, validation) {
    'use strict';

    var UnitModel = Backbone.Model.extend({
        defaults: {
        },

		validation: {
			zipCode: {
			  required: true
			},
			uf: {
			  maxLength: 2,
			  required: true
			},
			city: {
			  maxLength: 50,
			  required: true
			},
			neighborhood: {
			  maxLength: 50,
			  required: true
			},
			street: {
			  maxLength: 100,
			  required: true
			},
			number: {
			  maxLength: 7,
			  required: true
			}			
		}
    });

    return StudentModel;
});