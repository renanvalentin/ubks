/*global define*/

define([
    'underscore',
    'backbone',
    'moment'
], function (_, Backbone, moment) {
    'use strict';

    var StudentGraduationModel = Backbone.Model.extend({
        url: '/api/studentGraduation/',

        defaults: {
            examDate: moment().format('DD/MM/YYYY'),
            graduationId: 0,
            studentId: 0,
            degree: ''
        },

        validation: {
            examDate: {
                required: true,
                maxLength: 10,
                pattern: 'data'
            }
        }
    });

    return StudentGraduationModel;
});