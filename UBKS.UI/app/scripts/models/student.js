/*global define*/

define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    'use strict';

    var StudentModel = Backbone.Model.extend({
        url: '/api/student/',

        defaults: {
            codeTKFI: '',
            firstName: '',
            lastName: '',
            cpf: '',
            rg: '',
            birthDate: ''
        },

        validation: {
            firstName: {
                required: true
            },
            lastName: {
                required: true
            },
            codeTKFI: {
                maxLength: 30
            },
            cpf: {
                maxLength: 14
            },
            rg: {
                maxLength: 14
            },
            birthDate: {
                required: true,
                maxLength: 10,
                pattern: 'data'
            }
        }
    });

    return StudentModel;
});