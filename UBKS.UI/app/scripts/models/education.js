/*global define*/

define([
    'underscore',
    'backbone',
    'moment'
], function (_, Backbone, moment) {
    'use strict';

    var EducationModel = Backbone.Model.extend({
        defaults: {
            institution: '',
            course: '',
            period: '',
            grade: '',
            beginDate: ''
        },

        validation: {
            institution: {
                maxLength: 50
            },
            course: {
                maxLength: 50
            },
            period: {
                maxLength: 30
            },
            grade: {
                maxLength: 30
            },
            beginDate: {
                required: false,
                maxLength: 10,
                pattern: 'data'
            }
        }
    });

    return EducationModel;
});