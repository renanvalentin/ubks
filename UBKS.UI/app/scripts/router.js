/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'config',
    'presenter',
    'auth',
    'views/student/StudentListView',
    'views/student/StudentView',
    'views/student/StudentEditView',
    'views/graduation/GraduationListView',
    'views/graduation/GraduationView',
    'views/graduation/GraduationEditView',
    'models/student',
    'models/graduation',
    'collections/student',
    'collections/graduation'
], function ($, _, Backbone, config, presenter, auth, StudentListView, StudentView, StudentEditView, GraduationListView, GraduationView, GraduationEditView, StudentModel, GraduationModel, StudentCollection, GraduationCollection) {
    'use strict';

    var AppRouter = Backbone.Router.extend({
        routes: {
            '': 'listStudents',
            'student/': 'listStudents',
            'student/new': 'newStudent',
            'student/edit/:id': 'editStudent',
            'graduation/': 'graduations',
            'graduation/new': 'newGraduation',
            'graduation/edit/:id': 'editGraduation'
        },
        initialize: function () {
            $.ajaxPrefilter(function (options, originalOptions, jqXHR) {
                options.url = config.api + options.url;
                options.crossDomain = true;
                options.xhrFields = { withCredentials: false };
                jqXHR.setRequestHeader('Authorization', auth.getToken());
            });
        }
    });

    var authorize = function () {
        if (!auth.isAuthenticated())
            window.location = '/login.html';
    };

    var initialize = function () {
        var appRouter = new AppRouter();

        appRouter.on('route:listStudents', function () {
            authorize();

            var students = new StudentCollection();
            students.fetch().complete(function () {
                var view = new StudentListView({ collection: students });

                presenter.switchPage('Alunos', view);
            });
        });

        appRouter.on('route:newStudent', function () {
            authorize();

            var view = new StudentView();

            presenter.switchPage('Novo Aluno', view);
        });

        appRouter.on('route:editStudent', function (id) {
            authorize();

            var student = new StudentModel();
            student.fetch({ data: { id: id } }).complete(function () {
                var view = new StudentEditView({ model: new StudentModel(student.toJSON()) });

                presenter.switchPage('Aluno: ' + student.get('firstName'), view);
            });
        });

        appRouter.on('route:graduations', function () {
            authorize();

            var graduations = new GraduationCollection();
            graduations.fetch().complete(function () {
                var view = new GraduationListView({ collection: graduations });

                presenter.switchPage('Gradua��es', view);
            });
        });

        appRouter.on('route:newGraduation', function () {
            authorize();

            var view = new GraduationView();

            presenter.switchPage('Nova Gradua��o', view);
        });

        appRouter.on('route:editGraduation', function (id) {
            authorize();

            var graduation = new GraduationModel();
            graduation.fetch({ data: { id: id } }).complete(function () {
                var view = new GraduationEditView({ model: new GraduationModel(graduation.toJSON()) });

                presenter.switchPage('Gradua��o: ' + graduation.get('degree'), view);
            });
        });

        Backbone.history.start();
    };

    return {
        initialize: initialize
    };
});