﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using UBKS.Data.Configuration;
using UBKS.Models;

namespace UBKS.Data
{
    public class UBKSDbContext : DbContext
    {
        public UBKSDbContext()
            : base(nameOrConnectionString: "UBKS") { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Use singular table names
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Configurations.Add(new StudentConfiguration());
            modelBuilder.Configurations.Add(new StudentGraduationConfiguration());
            modelBuilder.Configurations.Add(new StudentContactConfiguration());

            modelBuilder.Configurations.Add(new CityConfiguration());
            modelBuilder.Configurations.Add(new CourseConfiguration());
            modelBuilder.Configurations.Add(new EducationConfiguration());

            modelBuilder.Configurations.Add(new StateConfiguration());
            modelBuilder.Configurations.Add(new GraduationConfiguration());

            modelBuilder.Configurations.Add(new EnrollmentConfiguration());
            modelBuilder.Configurations.Add(new OrganizationConfiguration());

            modelBuilder.Configurations.Add(new PresenceConfiguration());
            modelBuilder.Configurations.Add(new ResponsibleConfiguration());

            modelBuilder.Configurations.Add(new ClassConfiguration());
            modelBuilder.Configurations.Add(new UnitConfiguration());
            modelBuilder.Configurations.Add(new UnitContactConfiguration());

            modelBuilder.Configurations.Add(new PaymentConfiguration());

            modelBuilder.Configurations.Add(new UserConfiguration());
        }

        public DbSet<Student> Students { get; set; }
        public DbSet<StudentGraduation> StudentGraduations { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<StudentContact> StudentContact { get; set; }
        public DbSet<UnitContact> UnitContact { get; set; }

        public DbSet<Education> Educations { get; set; }

        public DbSet<Graduation> Graduations { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<Unit> Units { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Presence> Presences { get; set; }
        public DbSet<Responsible> Responsibles { get; set; }
        public DbSet<Class> Classes { get; set; }
        public DbSet<Course> Courses { get; set; }

        public DbSet<User> Users { get; set; }

    }
}
