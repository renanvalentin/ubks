namespace UBKS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Aluno",
                c => new
                    {
                        AlunoID = c.Long(nullable: false, identity: true),
                        CodeTKFI = c.String(maxLength: 30, unicode: false),
                        Logradouro = c.String(nullable: false, maxLength: 100, unicode: false),
                        Numero = c.String(nullable: false, maxLength: 7, unicode: false),
                        Bairro = c.String(nullable: false, maxLength: 50, unicode: false),
                        Cep = c.String(nullable: false, maxLength: 10, unicode: false),
                        Cidade = c.String(nullable: false, maxLength: 50),
                        Estado = c.String(nullable: false, maxLength: 2),
                        FirstName = c.String(nullable: false, maxLength: 30, unicode: false),
                        LastName = c.String(nullable: false, maxLength: 30, unicode: false),
                        BirthDate = c.DateTime(nullable: false),
                        Rg = c.String(maxLength: 16, unicode: false),
                        Cpf = c.String(maxLength: 16, unicode: false),
                        UnidadeID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.AlunoID)
                .ForeignKey("dbo.Unidade", t => t.UnidadeID, cascadeDelete: true)
                .Index(t => t.UnidadeID);
            
            CreateTable(
                "dbo.Responsavel",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        FirstName = c.String(nullable: false, maxLength: 30, unicode: false),
                        LastName = c.String(),
                        BirthDate = c.DateTime(nullable: false),
                        Rg = c.String(maxLength: 14, unicode: false),
                        Cpf = c.String(maxLength: 14, unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Aluno", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Unidade",
                c => new
                    {
                        UnidadeID = c.Long(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 50, unicode: false),
                        Logradouro = c.String(nullable: false, maxLength: 100, unicode: false),
                        Numero = c.String(nullable: false, maxLength: 7, unicode: false),
                        Bairro = c.String(nullable: false, maxLength: 50, unicode: false),
                        Cep = c.String(nullable: false, maxLength: 10, unicode: false),
                        Address_City = c.String(nullable: false, maxLength: 50),
                        Address_State = c.String(nullable: false, maxLength: 2),
                        OrganizacaoID = c.Long(nullable: false),
                        City_Id = c.Long(),
                    })
                .PrimaryKey(t => t.UnidadeID)
                .ForeignKey("dbo.Organizacao", t => t.OrganizacaoID, cascadeDelete: true)
                .ForeignKey("dbo.Cidade", t => t.City_Id)
                .Index(t => t.OrganizacaoID)
                .Index(t => t.City_Id);
            
            CreateTable(
                "dbo.Organizacao",
                c => new
                    {
                        OrganizacaoID = c.Long(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.OrganizacaoID);
            
            CreateTable(
                "dbo.Cidade",
                c => new
                    {
                        CidadeID = c.Long(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 50, unicode: false),
                        EstadoID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.CidadeID)
                .ForeignKey("dbo.Estado", t => t.EstadoID, cascadeDelete: true)
                .Index(t => t.EstadoID);
            
            CreateTable(
                "dbo.Estado",
                c => new
                    {
                        EstadoID = c.Long(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 30, unicode: false),
                        Sigla = c.String(nullable: false, maxLength: 2, unicode: false),
                    })
                .PrimaryKey(t => t.EstadoID);
            
            CreateTable(
                "dbo.Curso",
                c => new
                    {
                        CursoID = c.Long(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 50, unicode: false),
                        UnidadeID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.CursoID)
                .ForeignKey("dbo.Unidade", t => t.UnidadeID, cascadeDelete: true)
                .Index(t => t.UnidadeID);
            
            CreateTable(
                "dbo.Turma",
                c => new
                    {
                        TurmaID = c.Long(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 50, unicode: false),
                        DiasSemana = c.String(nullable: false, maxLength: 100, unicode: false),
                        DataInicio = c.DateTime(nullable: false),
                        DataTermino = c.DateTime(),
                        HorarioInicio = c.DateTime(nullable: false),
                        HorarioTermino = c.DateTime(nullable: false),
                        CursoID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.TurmaID)
                .ForeignKey("dbo.Curso", t => t.CursoID, cascadeDelete: true)
                .Index(t => t.CursoID);
            
            CreateTable(
                "dbo.Matricula",
                c => new
                    {
                        MatriculaID = c.Long(nullable: false, identity: true),
                        InscriptionDate = c.DateTime(nullable: false),
                        Scholarship = c.Boolean(nullable: false),
                        AlunoID = c.Long(nullable: false),
                        TurmaID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.MatriculaID)
                .ForeignKey("dbo.Aluno", t => t.AlunoID, cascadeDelete: true)
                .ForeignKey("dbo.Turma", t => t.TurmaID)
                .Index(t => t.AlunoID)
                .Index(t => t.TurmaID);
            
            CreateTable(
                "dbo.UnidadeContato",
                c => new
                    {
                        UnidadeContatoID = c.Long(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 50, unicode: false),
                        Telefone = c.String(nullable: false, maxLength: 15, unicode: false),
                        Celular = c.String(maxLength: 15, unicode: false),
                        Email = c.String(maxLength: 30, unicode: false),
                        UnidadeID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.UnidadeContatoID)
                .ForeignKey("dbo.Unidade", t => t.UnidadeID, cascadeDelete: true)
                .Index(t => t.UnidadeID);
            
            CreateTable(
                "dbo.Pagamento",
                c => new
                    {
                        PagamentoID = c.Long(nullable: false, identity: true),
                        PaymentDate = c.DateTime(nullable: false),
                        AlunoID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PagamentoID)
                .ForeignKey("dbo.Aluno", t => t.AlunoID, cascadeDelete: true)
                .Index(t => t.AlunoID);
            
            CreateTable(
                "dbo.AlunoContato",
                c => new
                    {
                        AlunoContatoID = c.Long(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 50, unicode: false),
                        Telefone = c.String(nullable: false, maxLength: 15, unicode: false),
                        Celular = c.String(maxLength: 15, unicode: false),
                        Email = c.String(maxLength: 30, unicode: false),
                        AlunoID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.AlunoContatoID)
                .ForeignKey("dbo.Aluno", t => t.AlunoID, cascadeDelete: true)
                .Index(t => t.AlunoID);
            
            CreateTable(
                "dbo.Presenca",
                c => new
                    {
                        PresencaID = c.Long(nullable: false, identity: true),
                        PresenceDate = c.DateTime(nullable: false),
                        IsPresent = c.Boolean(nullable: false),
                        AlunoID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PresencaID)
                .ForeignKey("dbo.Aluno", t => t.AlunoID, cascadeDelete: true)
                .Index(t => t.AlunoID);
            
            CreateTable(
                "dbo.StudentGraduation",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ExamDate = c.DateTime(nullable: false),
                        NumberDegreeIssued = c.String(),
                        AlunoID = c.Long(nullable: false),
                        GraduacaoID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Aluno", t => t.AlunoID, cascadeDelete: true)
                .ForeignKey("dbo.Graduacao", t => t.GraduacaoID, cascadeDelete: true)
                .Index(t => t.AlunoID)
                .Index(t => t.GraduacaoID);
            
            CreateTable(
                "dbo.Graduacao",
                c => new
                    {
                        GraduacaoID = c.Long(nullable: false, identity: true),
                        Degree = c.String(nullable: false, maxLength: 30, unicode: false),
                        StripColor = c.String(nullable: false, maxLength: 30, unicode: false),
                    })
                .PrimaryKey(t => t.GraduacaoID);
            
            CreateTable(
                "dbo.Escolaridade",
                c => new
                    {
                        EscolaridadeID = c.Long(nullable: false, identity: true),
                        Institution = c.String(maxLength: 50, unicode: false),
                        Course = c.String(nullable: false, maxLength: 50, unicode: false),
                        Period = c.String(maxLength: 30, unicode: false),
                        Grade = c.String(maxLength: 30, unicode: false),
                        BeginDate = c.DateTime(),
                        EndDate = c.DateTime(),
                        AlunoID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.EscolaridadeID)
                .ForeignKey("dbo.Aluno", t => t.AlunoID, cascadeDelete: true)
                .Index(t => t.AlunoID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Escolaridade", new[] { "AlunoID" });
            DropIndex("dbo.StudentGraduation", new[] { "GraduacaoID" });
            DropIndex("dbo.StudentGraduation", new[] { "AlunoID" });
            DropIndex("dbo.Presenca", new[] { "AlunoID" });
            DropIndex("dbo.AlunoContato", new[] { "AlunoID" });
            DropIndex("dbo.Pagamento", new[] { "AlunoID" });
            DropIndex("dbo.UnidadeContato", new[] { "UnidadeID" });
            DropIndex("dbo.Matricula", new[] { "TurmaID" });
            DropIndex("dbo.Matricula", new[] { "AlunoID" });
            DropIndex("dbo.Turma", new[] { "CursoID" });
            DropIndex("dbo.Curso", new[] { "UnidadeID" });
            DropIndex("dbo.Cidade", new[] { "EstadoID" });
            DropIndex("dbo.Unidade", new[] { "City_Id" });
            DropIndex("dbo.Unidade", new[] { "OrganizacaoID" });
            DropIndex("dbo.Responsavel", new[] { "Id" });
            DropIndex("dbo.Aluno", new[] { "UnidadeID" });
            DropForeignKey("dbo.Escolaridade", "AlunoID", "dbo.Aluno");
            DropForeignKey("dbo.StudentGraduation", "GraduacaoID", "dbo.Graduacao");
            DropForeignKey("dbo.StudentGraduation", "AlunoID", "dbo.Aluno");
            DropForeignKey("dbo.Presenca", "AlunoID", "dbo.Aluno");
            DropForeignKey("dbo.AlunoContato", "AlunoID", "dbo.Aluno");
            DropForeignKey("dbo.Pagamento", "AlunoID", "dbo.Aluno");
            DropForeignKey("dbo.UnidadeContato", "UnidadeID", "dbo.Unidade");
            DropForeignKey("dbo.Matricula", "TurmaID", "dbo.Turma");
            DropForeignKey("dbo.Matricula", "AlunoID", "dbo.Aluno");
            DropForeignKey("dbo.Turma", "CursoID", "dbo.Curso");
            DropForeignKey("dbo.Curso", "UnidadeID", "dbo.Unidade");
            DropForeignKey("dbo.Cidade", "EstadoID", "dbo.Estado");
            DropForeignKey("dbo.Unidade", "City_Id", "dbo.Cidade");
            DropForeignKey("dbo.Unidade", "OrganizacaoID", "dbo.Organizacao");
            DropForeignKey("dbo.Responsavel", "Id", "dbo.Aluno");
            DropForeignKey("dbo.Aluno", "UnidadeID", "dbo.Unidade");
            DropTable("dbo.Escolaridade");
            DropTable("dbo.Graduacao");
            DropTable("dbo.StudentGraduation");
            DropTable("dbo.Presenca");
            DropTable("dbo.AlunoContato");
            DropTable("dbo.Pagamento");
            DropTable("dbo.UnidadeContato");
            DropTable("dbo.Matricula");
            DropTable("dbo.Turma");
            DropTable("dbo.Curso");
            DropTable("dbo.Estado");
            DropTable("dbo.Cidade");
            DropTable("dbo.Organizacao");
            DropTable("dbo.Unidade");
            DropTable("dbo.Responsavel");
            DropTable("dbo.Aluno");
        }
    }
}
