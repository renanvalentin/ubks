﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBKS.Contracts;
using UBKS.Data.Helpers;
using UBKS.Models;

namespace UBKS.Data
{
    /// <summary>
    /// The UBKS "Unit of Work"
    ///     1) decouples the repos from the controllers
    ///     2) decouples the DbContext and EF from the controllers
    ///     3) manages the UoW
    /// </summary>
    /// <remarks>
    /// This class implements the "Unit of Work" pattern in which
    /// the "UoW" serves as a facade for querying and saving to the database.
    /// Querying is delegated to "repositories".
    /// Each repository serves as a container dedicated to a particular
    /// root entity type such as a <see cref="Class"/>.
    /// A repository typically exposes "Get" methods for querying and
    /// will offer add, update, and delete methods if those features are supported.
    /// The repositories rely on their parent UoW to provide the interface to the
    /// data layer (which is the EF DbContext in Code Camper).
    /// </remarks>
    public class UBKSUnitOfWork : IUBKSUnitOfWork, IDisposable
    {
        public UBKSUnitOfWork(IRepositoryProvider repositoryProvider)
        {
            CreateDbContext();

            repositoryProvider.DbContext = DbContext;
            RepositoryProvider = repositoryProvider;
        }

        /// <summary>
        /// Save pending changes to the database
        /// </summar>
        public void Commit()
        {
            //System.Diagnostics.Debug.WriteLine("Committed");
            DbContext.SaveChanges();
        }

        public IRepository<State> State { get { return GetStandardRepo<State>(); } }
        public IRepository<City> City { get { return GetStandardRepo<City>(); } }
        public IRepository<Student> Student { get { return GetStandardRepo<Student>(); } }
        public IRepository<Education> Education { get { return GetStandardRepo<Education>(); } }
        public IRepository<Responsible> Responsible { get { return GetStandardRepo<Responsible>(); } }
        public IRepository<Graduation> Graduation { get { return GetStandardRepo<Graduation>(); } }
        public IRepository<StudentGraduation> StudentGraduation { get { return GetStandardRepo<StudentGraduation>(); } }
        public IRepository<StudentContact> StudentContact { get { return GetStandardRepo<StudentContact>(); } }
        public IRepository<Unit> Unit { get { return GetStandardRepo<Unit>(); } }
        public IRepository<Payment> Payment { get { return GetStandardRepo<Payment>(); } }

        public IRepository<User> User { get { return GetStandardRepo<User>(); } }

        protected void CreateDbContext()
        {
            DbContext = new UBKSDbContext();

            DbContext.Configuration.ProxyCreationEnabled = false;
            DbContext.Configuration.LazyLoadingEnabled = false;
            DbContext.Configuration.ValidateOnSaveEnabled = false;
        }

        protected IRepositoryProvider RepositoryProvider { get; set; }

        private IRepository<T> GetStandardRepo<T>() where T : class
        {
            return RepositoryProvider.GetRepositoryForEntityType<T>();
        }

        private T GetRepo<T>() where T : class
        {
            return RepositoryProvider.GetRepository<T>();
        }

        private UBKSDbContext DbContext { get; set; }

        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (DbContext != null)
                {
                    DbContext.Dispose();
                }
            }
        }

        #endregion
    }
}
