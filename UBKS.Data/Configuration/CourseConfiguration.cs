﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBKS.Models;

namespace UBKS.Data.Configuration
{
    public class CourseConfiguration : EntityMappingBase<Course>
    {
        public CourseConfiguration()
            : base("CursoID", "Curso")
        {
            this.Property(t => t.Name).HasColumnName("Nome").HasColumnType("varchar").HasMaxLength(50).IsRequired();

            this.HasRequired(t => t.Unit)
                .WithMany(t => t.Courses)
                .Map(t => t.MapKey("UnidadeID"));
        }
    }
}
