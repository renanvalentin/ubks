﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBKS.Models;

namespace UBKS.Data.Configuration
{
    public class OrganizationConfiguration : EntityMappingBase<Organization>
    {
        public OrganizationConfiguration()
            : base("OrganizacaoID", "Organizacao")
        {
            this.Property(t => t.Name).HasColumnName("Nome").HasColumnType("varchar").HasMaxLength(50).IsRequired();
        }
    }
}
