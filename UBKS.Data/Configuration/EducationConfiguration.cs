﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBKS.Models;

namespace UBKS.Data.Configuration
{
    public class EducationConfiguration : EntityMappingBase<Education>
    {
        public EducationConfiguration()
            : base("EscolaridadeID", "Escolaridade")
        {
            this.Property(q => q.Course).HasColumnType("varchar").IsRequired().HasMaxLength(50);
            this.Property(q => q.Institution).HasColumnType("varchar").HasMaxLength(50);
            this.Property(q => q.Period).HasColumnType("varchar").HasMaxLength(30);
            this.Property(q => q.Grade).HasColumnType("varchar").HasMaxLength(30);
            this.Property(q => q.BeginDate).IsOptional();
            this.Property(q => q.EndDate).IsOptional();
            
            this.HasRequired(t => t.Student)
                     .WithMany(t => t.Educations)
                     .Map(t => t.MapKey("AlunoID"));
        }
    }
}
