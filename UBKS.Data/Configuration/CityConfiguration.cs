﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBKS.Models;

namespace UBKS.Data.Configuration
{
    public class CityConfiguration : EntityMappingBase<City>
    {
        public CityConfiguration()
            : base("CidadeID", "Cidade")
        {
            this.Property(t => t.Name).HasColumnName("Nome").HasColumnType("varchar").HasMaxLength(50).IsRequired();

            this.HasRequired(t => t.State)
                .WithMany(t => t.Cities)
                .Map(t => t.MapKey("EstadoID"));
        }
    }
}
