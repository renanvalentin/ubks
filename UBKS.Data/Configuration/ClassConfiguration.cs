﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBKS.Models;

namespace UBKS.Data.Configuration
{
    public class ClassConfiguration : EntityMappingBase<Class>
    {
        public ClassConfiguration()
            : base("TurmaID", "Turma")
        {
            this.Property(t => t.Nome).HasColumnName("Nome").HasColumnType("varchar").HasMaxLength(50).IsRequired();
            this.Property(t => t.DaysOfWeek).HasColumnName("DiasSemana").HasColumnType("varchar").HasMaxLength(100).IsRequired();
            this.Property(t => t.BeginDate).HasColumnName("DataInicio").IsRequired();
            this.Property(t => t.EndDate).HasColumnName("DataTermino").IsOptional();
            this.Property(t => t.BeginTime).HasColumnName("HorarioInicio").IsRequired();
            this.Property(t => t.EndTime).HasColumnName("HorarioTermino").IsRequired();
            
            this.HasRequired(t => t.Course)
                .WithMany(t => t.Classes)
                .Map(t => t.MapKey("CursoID"));
        }
    }
}
