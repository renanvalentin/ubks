﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBKS.Models;

namespace UBKS.Data.Configuration
{
    public class PaymentConfiguration : EntityMappingBase<Payment>
    {
        public PaymentConfiguration()
            : base("PagamentoID", "Pagamento")
        {
            this.Property(q => q.PaymentDate).IsRequired();

            this.HasRequired(t => t.Student)
                .WithMany(t => t.Payments)
                .Map(t => t.MapKey("AlunoID"));
        }
    }
}
