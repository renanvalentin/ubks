﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBKS.Models;

namespace UBKS.Data.Configuration
{
    public class StudentContactConfiguration : EntityMappingBase<StudentContact>
    {
        public StudentContactConfiguration()
            : base("AlunoContatoID", "AlunoContato")
        {
            this.Property(t => t.Name).HasColumnName("Nome").HasColumnType("varchar").HasMaxLength(50).IsRequired();
            this.Property(t => t.CellPhone).HasColumnName("Celular").HasColumnType("varchar").HasMaxLength(15).IsOptional();
            this.Property(t => t.Phone).HasColumnName("Telefone").HasColumnType("varchar").HasMaxLength(15).IsRequired();
            this.Property(t => t.Email).HasColumnName("Email").HasColumnType("varchar").HasMaxLength(30).IsOptional();

            this.HasRequired(t => t.Student)
                .WithMany(t => t.Contacts)
                .Map(t => t.MapKey("AlunoID"));
        }
    }

    public class UnitContactConfiguration : EntityMappingBase<UnitContact>
    {
        public UnitContactConfiguration()
            : base("UnidadeContatoID", "UnidadeContato")
        {
            this.Property(t => t.Name).HasColumnName("Nome").HasColumnType("varchar").HasMaxLength(50).IsRequired();
            this.Property(t => t.CellPhone).HasColumnName("Celular").HasColumnType("varchar").HasMaxLength(15).IsOptional();
            this.Property(t => t.Phone).HasColumnName("Telefone").HasColumnType("varchar").HasMaxLength(15).IsRequired();
            this.Property(t => t.Email).HasColumnName("Email").HasColumnType("varchar").HasMaxLength(30).IsOptional();

            this.HasRequired(t => t.Unit)
                .WithMany(t => t.Contacts)
                .Map(t => t.MapKey("UnidadeID"));
        }
    }
}
