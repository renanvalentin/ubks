﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBKS.Models;

namespace UBKS.Data.Configuration
{
    public class PresenceConfiguration : EntityMappingBase<Presence>
    {
        public PresenceConfiguration()
            : base("PresencaID", "Presenca")
        {
            this.Property(q => q.IsPresent).IsRequired();
            this.Property(q => q.PresenceDate).IsRequired();

            this.HasRequired(t => t.Student)
                .WithMany(t => t.Presences)
                .Map(t => t.MapKey("AlunoID"));
        }
    }
}
