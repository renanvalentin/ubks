﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBKS.Models;

namespace UBKS.Data.Configuration
{
    public class StudentGraduationConfiguration : EntityMappingBase<StudentGraduation>
    {
        public StudentGraduationConfiguration()
        {
            this.HasRequired(t => t.Student)
                .WithMany(t => t.StudentGraduations)
                .Map(t => t.MapKey("AlunoID"));

            this.HasRequired(t => t.Graduation)
                .WithMany(t => t.StudentGraduation)
                .Map(t => t.MapKey("GraduacaoID"));
        }
    }
}
