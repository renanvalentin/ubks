﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBKS.Models;

namespace UBKS.Data.Configuration
{
    public class GraduationConfiguration : EntityMappingBase<Graduation>
    {
        public GraduationConfiguration()
            : base("GraduacaoID", "Graduacao")
        {
            this.Property(q => q.StripColor).HasColumnType("varchar").IsRequired().HasMaxLength(30);
            this.Property(q => q.Degree).HasColumnType("varchar").IsRequired().HasMaxLength(30);
        }

    }
}
