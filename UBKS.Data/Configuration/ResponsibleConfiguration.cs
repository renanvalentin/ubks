﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBKS.Models;

namespace UBKS.Data.Configuration
{
    public class ResponsibleConfiguration : EntityTypeConfiguration<Responsible>
    {
        public ResponsibleConfiguration()
        {
            this.Property(q => q.FirstName).HasColumnType("varchar").IsRequired().HasMaxLength(30);
            this.Property(q => q.Cpf).HasColumnType("varchar").HasMaxLength(14);
            this.Property(q => q.Rg).HasColumnType("varchar").HasMaxLength(14);
            this.Property(q => q.BirthDate).IsRequired();

            this.HasRequired(t => t.Student)
                .WithOptional(t => t.Responsible);

            this.ToTable("Responsavel");
        }
    }
}
