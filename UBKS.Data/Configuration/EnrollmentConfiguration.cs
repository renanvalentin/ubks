﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBKS.Models;

namespace UBKS.Data.Configuration
{
    public class EnrollmentConfiguration : EntityMappingBase<Enrollment>
    {
        public EnrollmentConfiguration()
            : base("MatriculaID", "Matricula")
        {
            this.Property(q => q.Scholarship).IsRequired();
            this.Property(q => q.InscriptionDate).IsRequired();

            this.HasRequired(q => q.Student)
                .WithMany()
                .Map(t => t.MapKey("AlunoID"));

            this.HasRequired(t => t.Class)
                .WithMany(t => t.Enrollments)
                .Map(t => t.MapKey("TurmaID"))
                .WillCascadeOnDelete(false);
        }
    }
}
