﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBKS.Models;

namespace UBKS.Data.Configuration
{
    public abstract class EntityMappingBase<T> : EntityTypeConfiguration<T> where T : Entity
    {
        public EntityMappingBase()
        {
            HasKey(x => x.Id);

            Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }

        public EntityMappingBase(string columnName)
            : this()
        {
            Property(x => x.Id).HasColumnName(columnName);
        }

        public EntityMappingBase(string columnName, string tableName)
            : this(columnName)
        {
            ToTable(tableName);
        }
    }
}
