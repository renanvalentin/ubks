﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBKS.Models;

namespace UBKS.Data.Configuration
{
    public class StudentConfiguration : EntityMappingBase<Student>
    {
        public StudentConfiguration()
            : base("AlunoID", "Aluno")
        {
            this.Property(q => q.FirstName).HasColumnType("varchar").IsRequired().HasMaxLength(30);
            this.Property(q => q.LastName).HasColumnType("varchar").IsRequired().HasMaxLength(30);
            this.Property(q => q.CodeTKFI).HasColumnType("varchar").HasMaxLength(30);
            this.Property(q => q.Cpf).HasColumnType("varchar").HasMaxLength(16);
            this.Property(q => q.Rg).HasColumnType("varchar").HasMaxLength(16);
            this.Property(q => q.BirthDate).IsRequired();

            this.Property(t => t.Address.State).HasColumnName("Estado").HasMaxLength(2).IsRequired();
            this.Property(t => t.Address.City).HasColumnName("Cidade").HasMaxLength(50).IsRequired();
            this.Property(t => t.Address.Neighborhood).HasColumnName("Bairro").HasColumnType("varchar").HasMaxLength(50).IsRequired();
            this.Property(t => t.Address.ZipCode).HasColumnName("Cep").HasColumnType("varchar").HasMaxLength(10).IsRequired();
            this.Property(t => t.Address.Street).HasColumnName("Logradouro").HasColumnType("varchar").HasMaxLength(100).IsRequired();
            this.Property(t => t.Address.Number).HasColumnName("Numero").HasColumnType("varchar").HasMaxLength(7).IsRequired();

            this.HasRequired(t => t.Unit)
                .WithMany(t => t.Students)
                .Map(t => t.MapKey("UnidadeID"));
        }
    }
}
