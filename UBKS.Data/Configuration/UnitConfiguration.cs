﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBKS.Models;

namespace UBKS.Data.Configuration
{
    public class UnitConfiguration : EntityMappingBase<Unit>
    {
        public UnitConfiguration()
            : base("UnidadeID", "Unidade")
        {
            this.Property(t => t.Name).HasColumnName("Nome").HasColumnType("varchar").HasMaxLength(50).IsRequired();

            this.Property(t => t.Address.Neighborhood).HasColumnName("Bairro").HasColumnType("varchar").HasMaxLength(50).IsRequired();
            this.Property(t => t.Address.ZipCode).HasColumnName("Cep").HasColumnType("varchar").HasMaxLength(10).IsRequired();
            this.Property(t => t.Address.Street).HasColumnName("Logradouro").HasColumnType("varchar").HasMaxLength(100).IsRequired();
            this.Property(t => t.Address.Number).HasColumnName("Numero").HasColumnType("varchar").HasMaxLength(7).IsRequired();

            this.HasRequired(t => t.Organization)
                .WithMany(t => t.Units)
                .Map(t => t.MapKey("OrganizacaoID"));
        }
    }
}
