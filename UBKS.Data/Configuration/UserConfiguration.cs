﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBKS.Models;

namespace UBKS.Data.Configuration
{
    public class UserConfiguration : EntityMappingBase<User>
    {
        public UserConfiguration()
            : base("UsuarioID", "Usuario")
        {
            this.Property(t => t.Login).HasColumnName("Login").HasColumnType("varchar").HasMaxLength(50).IsRequired();
            this.Property(t => t.Password).HasColumnName("Senha").HasColumnType("varchar").HasMaxLength(20).IsRequired();
        }
    }
}