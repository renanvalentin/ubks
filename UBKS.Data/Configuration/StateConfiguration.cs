﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBKS.Models;

namespace UBKS.Data.Configuration
{
    public class StateConfiguration : EntityMappingBase<State>
    {
        public StateConfiguration()
            : base("EstadoID", "Estado")
        {
            this.Property(t => t.Name).HasColumnName("Nome").HasColumnType("varchar").HasMaxLength(30).IsRequired();
            this.Property(t => t.Initials).HasColumnName("Sigla").HasColumnType("varchar").HasMaxLength(2).IsRequired();
        }
    }
}
