﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UBKS.Models;

namespace UBKS.WebApi.Models
{
    public class StudentDto
    {

        public StudentDto()
        {
            Educations = new List<EducationDto>();
            Contacts = new List<ContactDto>();
        }
        public StudentDto(Student student)
            : this()
        {
            Id = student.Id;
            FirstName = student.FirstName;
            LastName = student.LastName;
            BirthDate = student.BirthDate.ToShortDateString();
            Rg = student.Rg;
            Cpf = student.Cpf;
            CodeTKFI = student.CodeTKFI;

            foreach (var item in student.Educations)
                Educations.Add(new EducationDto(item));

            foreach (var item in student.Contacts)
                Contacts.Add(new ContactDto(item));

            Address = new AddressDto(student.Address);

            if (student.Responsible != null)
            {
                student.Responsible.Id = student.Id;
                Responsible = new ResponsibleDto(student.Responsible);
            }
        }

        public long Id { get; set; }
        public long Unit { get; set; }
        public string CodeTKFI { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string BirthDate { get; set; }
        public string Rg { get; set; }
        public string Cpf { get; set; }

        public AddressDto Address { get; set; }

        public ResponsibleDto Responsible { get; set; }

        public List<ContactDto> Contacts { get; set; }
        public List<EducationDto> Educations { get; set; }

        public Student ToEntity()
        {
            var student = new Student
            {
                Id = Id,
                Unit = new UBKS.Models.Unit
                {
                    Id = Unit
                },
                FirstName = FirstName,
                LastName = LastName,
                CodeTKFI = CodeTKFI,
                BirthDate = Convert.ToDateTime(BirthDate),
                Address = Address.ToEntity(),
                Rg = Rg,
                Cpf = Cpf
            };

            foreach (var item in Contacts)
                student.Contacts.Add(item.ToStudentEntity());

            foreach (var item in Educations)
                student.Educations.Add(item.ToEntity());

            if(student.Responsible != null)
            {
                student.Responsible = Responsible.ToResponsibleEntity();
                student.Responsible.Id = student.Id;
            }

            

            return student;
        }
    }
}