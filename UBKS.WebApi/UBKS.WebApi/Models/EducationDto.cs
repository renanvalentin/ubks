﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UBKS.Models;

namespace UBKS.WebApi.Models
{
    public class EducationDto
    {
        public EducationDto() { }
        public EducationDto(Education education)
        {
            Id = education.Id;
            Institution = education.Institution;
            Course = education.Course;
            Period = education.Period;
            Grade = education.Grade;
            BeginDate = education.BeginDate.HasValue ? education.BeginDate.Value.ToShortDateString() : null;
            EndDate = education.EndDate.HasValue ? education.EndDate.Value.ToShortDateString() : null;
        }

        public long Id { get; set; }
        public string Institution { get; set; }
        public string Course { get; set; }
        public string Period { get; set; }
        public string Grade { get; set; }
        public string BeginDate { get; set; }
        public string EndDate { get; set; }

        public Education ToEntity()
        {
            var education = new Education
            {
                Id = Id,
                Institution = Institution,
                Course = Course,
                Period = Period,
                Grade = Grade
            };

            if (!string.IsNullOrEmpty(BeginDate))
                education.BeginDate = Convert.ToDateTime(BeginDate);

            if (!string.IsNullOrEmpty(EndDate))
                education.EndDate = Convert.ToDateTime(EndDate);

            return education;
        }
    }
}