﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UBKS.Models;

namespace UBKS.WebApi.Models
{
    public class StudentGraduationDto
    {
        public StudentGraduationDto() { }

        public StudentGraduationDto(StudentGraduation studentGraduation)
        {
            ExamDate = studentGraduation.ExamDate.ToShortDateString();
            Degree = studentGraduation.Graduation.Degree;
            //GraduationId = studentGraduation.Graduation.Id;
            //StudentId = studentGraduation.Student.Id;
        }

        public string ExamDate { get; set; }
        public string Degree { get; set; }
        public long GraduationId { get; set; }
        public long StudentId { get; set; }

        public StudentGraduation ToEntity()
        {
            return new StudentGraduation
            {
                ExamDate = Convert.ToDateTime(ExamDate),
                Graduation = new Graduation
                {
                    Id = GraduationId
                },
                Student = new Student
                {
                    Id = StudentId
                }
            };
        }
    }
}