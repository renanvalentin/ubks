﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UBKS.Models;

namespace UBKS.WebApi.Models
{
    public class PaymentDto
    {
        public PaymentDto() { }
        public PaymentDto(Payment payment)
        {
            Id = payment.Id;
            PaymentDate = payment.PaymentDate.ToShortDateString();
            StudentId = payment.Student.Id;
        }

        public long Id { get; set; }
        public long StudentId { get; set; }
        public string PaymentDate { get; set; }

        public Payment ToEntity()
        {
            return new Payment
            {
                Id = Id,
                PaymentDate = Convert.ToDateTime(PaymentDate),
                Student = new Student
                {
                    Id = StudentId
                }
            };
        }
    }
}