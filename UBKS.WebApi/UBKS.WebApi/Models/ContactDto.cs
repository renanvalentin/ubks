﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UBKS.Models;

namespace UBKS.WebApi.Models
{
    public class ContactDto
    {
        public ContactDto() { }
        public ContactDto(StudentContact contact)
        {
            Id = contact.Id;
            Name = contact.Name;
            Phone = contact.Phone;
            CellPhone = contact.CellPhone;
            Email = contact.Email;
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string CellPhone { get; set; }
        public string Email { get; set; }

        public StudentContact ToStudentEntity()
        {
            return new StudentContact
            {
                Id = Id,
                Name = Name,
                Phone = Phone,
                CellPhone = CellPhone,
                Email = Email
            };
        }
    }
}