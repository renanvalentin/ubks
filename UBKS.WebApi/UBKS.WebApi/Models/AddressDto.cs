﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UBKS.Models;

namespace UBKS.WebApi.Models
{
    public class AddressDto
    {
        public AddressDto() { }
        public AddressDto(Address address)
        {
            Street = address.Street;
            Number = address.Number;
            Neighborhood = address.Neighborhood;
            ZipCode = address.ZipCode;
            City = address.City;
            State = address.State;
        }

        public string Street { get; set; }
        public string Number { get; set; }
        public string Neighborhood { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string State { get; set; }

        public Address ToEntity()
        {
            return new Address
            {
                City = City,
                Neighborhood = Neighborhood,
                Number = Number,
                State = State,
                Street = Street,
                ZipCode = ZipCode
            };
        }
    }
}