﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UBKS.Models;

namespace UBKS.WebApi.Models
{
    public class ResponsibleDto
    {
        public ResponsibleDto() { }
        public ResponsibleDto(Responsible responsible)
        {
            FirstName = responsible.FirstName;
            LastName = responsible.LastName;
            BirthDate = responsible.BirthDate.ToShortDateString();
            Rg = responsible.Rg;
            Cpf = responsible.Cpf;
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string BirthDate { get; set; }
        public string Rg { get; set; }
        public string Cpf { get; set; }

        public Responsible ToResponsibleEntity()
        {
            return new Responsible
            {
                FirstName = FirstName,
                LastName = LastName,
                BirthDate = Convert.ToDateTime(BirthDate),
                Rg = Rg,
                Cpf = Cpf
            };
        }
    }
}