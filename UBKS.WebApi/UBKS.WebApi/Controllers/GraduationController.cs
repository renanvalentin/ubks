﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UBKS.Contracts;
using UBKS.Models;
using System.Data.Entity;
using System.Linq.Expressions;
using System.Threading;
using UBKS.WebApi.Models;

namespace UBKS.WebApi.Controllers
{
    public class GraduationController : ApiBaseController
    {
        public GraduationController()
        {
        }

        public GraduationController(IUBKSUnitOfWork uow)
            : base(uow)
        { }

        // GET api/student
        public IEnumerable<Graduation> Get()
        {
            var graduations = Uow.Graduation.GetAll().ToList();
            return graduations;
        }

        // GET api/student/5
        public Graduation Get(int id)
        {
            var graduation = Uow.Graduation.GetById(id);
            return graduation;
        }

        // POST api/student
        public HttpResponseMessage Post(Graduation graduation)
        {
            if (ModelState.IsValid)
            {
                Uow.Graduation.Add(graduation);
                Uow.Commit();

                var response = Request.CreateResponse(HttpStatusCode.Created, graduation);

                response.Headers.Location =
                    new Uri(Url.Link(WebApiConfig.ControllerAndId, new { id = graduation.Id }));

                return response;
            }

            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }

        // PUT api/student/5
        public HttpResponseMessage Put(Graduation graduation)
        {
            if (ModelState.IsValid)
            {
                Uow.Graduation.Update(graduation);

                Uow.Commit();
            }

            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }

        // DELETE api/student/5
        public HttpResponseMessage Delete(int id)
        {
            Uow.Graduation.Delete(id);
            Uow.Commit();

            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }
    }
}
