﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using UBKS.Contracts;
using UBKS.Models;

namespace UBKS.WebApi.Controllers
{
    public class UserController : ApiBaseController
    {
        public UserController()
        { }

        public UserController(IUBKSUnitOfWork uow)
            : base(uow)
        { }

        public HttpResponseMessage Login(User user)
        {
            var auth = Uow.User.GetBy(q => q.Login == user.Login && q.Password == user.Password);

            if (auth != null)
            {
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(user.Login + ":" + user.Password);
                var encoded = System.Convert.ToBase64String(plainTextBytes);

                return Request.CreateResponse(HttpStatusCode.Created, encoded);
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest, "Login ou senha inválidos!");
        }
    }
}
