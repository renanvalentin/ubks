﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UBKS.Contracts;
using UBKS.Data;
using UBKS.Data.Helpers;
using UBKS.Models;

namespace UBKS.WebApi.Controllers
{
    public class CityController : ApiBaseController
    {
        public CityController(IUBKSUnitOfWork uow)
            : base(uow)
        { }

        // GET api/city
        public IEnumerable<City> Get()
        {
            return Uow.City.GetAll();
        }

        // GET api/city/5
        public City Get(int id)
        {
            return Uow.City.GetById(id);
        }

        // POST api/city
        public HttpResponseMessage Post(City city)
        {
            if (ModelState.IsValid)
            {
                Uow.City.Add(city);
                Uow.Commit();

                var response = Request.CreateResponse(HttpStatusCode.Created, city);

                response.Headers.Location =
                    new Uri(Url.Link(WebApiConfig.ControllerAndId, new { id = city.Id }));

                return response;
            }

            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }

        // PUT api/city/5
        public HttpResponseMessage Put(City city)
        {
            Uow.City.Update(city);
            Uow.Commit();

            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }

        // DELETE api/city/5
        public HttpResponseMessage Delete(int id)
        {
            Uow.City.Delete(id);
            Uow.Commit();

            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }
    }
}
