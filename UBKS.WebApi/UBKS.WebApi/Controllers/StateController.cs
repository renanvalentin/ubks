﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UBKS.Contracts;
using UBKS.Models;

namespace UBKS.WebApi.Controllers
{
    public class StateController : ApiBaseController
    {
        public StateController(IUBKSUnitOfWork uow)
            : base(uow)
        { }

        // GET api/state
        public IEnumerable<State> Get()
        {
            return Uow.State.GetAll();
        }

        // GET api/cidade/5
        public State Get(int id)
        {
            return Uow.State.GetById(id);
        }

        // POST api/cidade
        public HttpResponseMessage Post(State state)
        {
            if (ModelState.IsValid)
            {
                Uow.State.Add(state);
                Uow.Commit();

                var response = Request.CreateResponse(HttpStatusCode.Created, state);

                response.Headers.Location =
                    new Uri(Url.Link(WebApiConfig.ControllerAndId, new { id = state.Id }));

                return response;
            }

            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }

        // PUT api/cidade/5
        public HttpResponseMessage Put(State state)
        {
            Uow.State.Update(state);
            Uow.Commit();

            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }

        // DELETE api/cidade/5
        public HttpResponseMessage Delete(int id)
        {
            Uow.State.Delete(id);
            Uow.Commit();

            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }
    }
}
