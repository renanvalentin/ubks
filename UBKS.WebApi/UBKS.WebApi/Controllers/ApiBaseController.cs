﻿using System.Web.Http;
using UBKS.Contracts;
using UBKS.Data;
using UBKS.Data.Helpers;

namespace UBKS.WebApi.Controllers
{
    public class ApiBaseController : ApiController
    {
        protected UBKS.Contracts.IUBKSUnitOfWork Uow { get; set; }

        public ApiBaseController()
        {
            Uow = new UBKSUnitOfWork(new RepositoryProvider(new RepositoryFactories()));
        }

        public ApiBaseController(IUBKSUnitOfWork uow)
        {
            Uow = uow;
        }
    }
}
