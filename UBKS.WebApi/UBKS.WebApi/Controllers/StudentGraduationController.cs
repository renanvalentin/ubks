﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UBKS.Contracts;
using UBKS.Models;
using System.Data.Entity;
using System.Linq.Expressions;
using System.Threading;
using UBKS.WebApi.Models;

namespace UBKS.WebApi.Controllers
{
    public class StudentGraduationController : ApiBaseController
    {
        public StudentGraduationController()
        {
        }

        public StudentGraduationController(IUBKSUnitOfWork uow)
            : base(uow)
        { }

        // GET api/student
        public IEnumerable<StudentGraduationDto> Get(int id)
        {
            var studentGraduation = Uow.StudentGraduation.GetAll(q => q.Student.Id == id, q => q.Graduation).ToList();
            var studentGraduationDto = new List<StudentGraduationDto>();

            foreach (var item in studentGraduation)
            {
                studentGraduationDto.Add(new StudentGraduationDto(item));
            }

            return studentGraduationDto;
        }

        // POST api/student
        public HttpResponseMessage Post(StudentGraduationDto studentGraduationDto)
        {
            if (ModelState.IsValid)
            {
                var studentGraduation = studentGraduationDto.ToEntity();

                studentGraduation.Graduation = Uow.Graduation.GetById(studentGraduationDto.GraduationId);
                studentGraduation.Student = Uow.Student.GetById(studentGraduationDto.StudentId);

                Uow.StudentGraduation.Add(studentGraduation);
                Uow.Commit();

                var response = Request.CreateResponse(HttpStatusCode.Created, studentGraduation);

                response.Headers.Location =
                    new Uri(Url.Link(WebApiConfig.ControllerAndId, new { id = studentGraduation.Id }));

                return response;
            }

            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }

        // PUT api/student/5
        public HttpResponseMessage Put(Graduation graduation)
        {
            throw new NotImplementedException();

            if (ModelState.IsValid)
            {
                Uow.Graduation.Update(graduation);

                Uow.Commit();
            }

            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }

        // DELETE api/student/5
        public HttpResponseMessage Delete(int id)
        {
            throw new NotImplementedException();

            Uow.Graduation.Delete(id);
            Uow.Commit();

            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }
    }
}
