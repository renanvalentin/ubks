﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UBKS.Contracts;
using UBKS.Models;
using System.Data.Entity;
using System.Linq.Expressions;
using System.Threading;
using UBKS.WebApi.Models;

namespace UBKS.WebApi.Controllers
{
    public class StudentController : ApiBaseController
    {
        public StudentController()
        {
        }

        public StudentController(IUBKSUnitOfWork uow)
            : base(uow)
        { }

        [Authorize]
        public IEnumerable<Student> Get()
        {
            var students = Uow.Student.GetAll(q => q.Contacts).ToList();
            return students;
        }

        // GET api/student/5
        public StudentDto Get(int id)
        {
            var student = Uow.Student.GetAll(q => q.Contacts, q => q.Educations, q => q.Responsible).FirstOrDefault(q => q.Id == id);
            return new StudentDto(student);
        }

        // POST api/student
        public HttpResponseMessage Post(StudentDto studentDto)
        {
            if (ModelState.IsValid)
            {
                var student = studentDto.ToEntity();

                student.Unit = Uow.Unit.GetById(student.Unit.Id);

                Uow.Student.Add(student);
                Uow.Commit();

                var response = Request.CreateResponse(HttpStatusCode.Created, student);

                response.Headers.Location =
                    new Uri(Url.Link(WebApiConfig.ControllerAndId, new { id = student.Id }));

                return response;
            }

            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }

        // PUT api/student/5
        public HttpResponseMessage Put(StudentDto student)
        {
            if (ModelState.IsValid)
            {
                var _student = student.ToEntity();

                Uow.Student.Update(_student);

                if (_student.Responsible != null)
                {
                    var responsible = Uow.Responsible.GetById(_student.Id);

                    if (responsible != null)
                        Uow.Responsible.Update(_student.Responsible);
                    else
                        Uow.Responsible.Add(_student.Responsible);
                }

                foreach (var item in _student.Educations)
                    Uow.Education.Update(item);

                foreach (var item in _student.Contacts)
                    Uow.StudentContact.Update(item);

                Uow.Commit();
            }

            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }

        // DELETE api/student/5
        public HttpResponseMessage Delete(int id)
        {
            Uow.Student.Delete(id);
            Uow.Commit();

            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }
    }
}
