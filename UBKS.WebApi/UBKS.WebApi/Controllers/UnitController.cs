﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UBKS.Contracts;
using UBKS.Models;

namespace UBKS.WebApi.Controllers
{
    public class UnitController : ApiBaseController
    {
        public UnitController(IUBKSUnitOfWork uow)
            : base(uow)
        { }

        // GET api/unit
        public IEnumerable<Unit> Get()
        {
            return Uow.Unit.GetAll();
        }

        // GET api/unit/5
        public Unit Get(int id)
        {
            return Uow.Unit.GetById(id);
        }

        // POST api/unit
        public HttpResponseMessage Post(Unit unit)
        {
            if (ModelState.IsValid)
            {
                Uow.Unit.Add(unit);
                Uow.Commit();

                var response = Request.CreateResponse(HttpStatusCode.Created, unit);

                response.Headers.Location =
                    new Uri(Url.Link(WebApiConfig.ControllerAndId, new { id = unit.Id }));

                return response;
            }

            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }

        // PUT api/unit/5
        public HttpResponseMessage Put(Unit unit)
        {
            Uow.Unit.Update(unit);
            Uow.Commit();

            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }

        // DELETE api/unit/5
        public HttpResponseMessage Delete(int id)
        {
            Uow.Unit.Delete(id);
            Uow.Commit();

            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }
    }
}
