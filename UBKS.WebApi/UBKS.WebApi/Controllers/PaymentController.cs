﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UBKS.Contracts;
using UBKS.Models;
using System.Data.Entity;
using System.Linq.Expressions;
using System.Threading;
using UBKS.WebApi.Models;

namespace UBKS.WebApi.Controllers
{
    public class PaymentController : ApiBaseController
    {
        public PaymentController()
        {
        }

        public PaymentController(IUBKSUnitOfWork uow)
            : base(uow)
        { }

        // GET api/student
        public IEnumerable<PaymentDto> Get(int id)
        {
            var payments = Uow.Payment.GetAll(q => q.Student.Id == id, q => q.Student).ToList();
            var paymentDtos = new List<PaymentDto>();

            foreach (var item in payments)
                paymentDtos.Add(new PaymentDto(item));

            return paymentDtos;
        }

        // POST api/student
        public HttpResponseMessage Post(PaymentDto paymentDto)
        {
            if (ModelState.IsValid)
            {
                var payment = paymentDto.ToEntity();
                payment.Student = Uow.Student.GetById(paymentDto.StudentId);

                Uow.Payment.Add(payment);
                Uow.Commit();

                var response = Request.CreateResponse(HttpStatusCode.Created, payment);

                response.Headers.Location =
                    new Uri(Url.Link(WebApiConfig.ControllerAndId, new { id = payment.Id }));

                return response;
            }

            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }
    }
}
