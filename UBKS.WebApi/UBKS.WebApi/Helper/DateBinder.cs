﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UBKS.WebApi.Helper
{
    public class DateTimeModelBinder : DefaultModelBinder
    {
        private string _customFormat;

        public DateTimeModelBinder()
        {
            _customFormat = "dd/MM/yyyy";
        }

        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            return DateTime.ParseExact(value.AttemptedValue, _customFormat, CultureInfo.InvariantCulture);
        }
    }

    public class UBKSModelBinder : DefaultModelBinder
    {
        private string _customFormat;

        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if(bindingContext.ValueProvider.GetType() == typeof(DateTime)) {

            }

            return base.BindModel(controllerContext, bindingContext);
        }
    }
}