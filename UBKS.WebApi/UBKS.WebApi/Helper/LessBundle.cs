﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace UBKS.WebApi.Helper
{
    public class LessBundle : Bundle
    {
        public LessBundle(string virtualPath)
        {
            this.Path = virtualPath;

            this.Transforms.Add(new LessTransform());
            this.Transforms.Add(new CssMinify());
        }

        public LessBundle(string virtualPath, string cdnPath)
            : this(virtualPath)
        {
            this.CdnPath = cdnPath;
        }
    }
}