﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UBKS.WebApi.Helper
{
    public class UBKSViewEngine : RazorViewEngine
    {
        public UBKSViewEngine()
        {
            var newLocationFormat = new[]
                                    {
                                        "~/Views/{0}.cshtml",
                                        "~/Views/{1}/Partial/{0}.cshtml"
                                    };

            PartialViewLocationFormats = PartialViewLocationFormats.Union(newLocationFormat).ToArray();
        }
    }
}