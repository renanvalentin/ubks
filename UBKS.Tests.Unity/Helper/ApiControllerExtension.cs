﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Hosting;
using System.Web.Http.Routing;
using UBKS.WebApi;

namespace UBKS.Tests.Unity.Helper
{
    public static class ApiControllerExtensions
    {
        public static void MockRequest(this ApiController self, string routeName, string uri, HttpMethod httpMethod, object routeValues)
        {
            var routeValueDict = new HttpRouteValueDictionary();
            foreach (var prop in routeValues.GetType().GetProperties())
            {
                routeValueDict.Add(prop.Name, prop.GetValue(routeValues, null));
            }

            var httpConfiguration = new HttpConfiguration();
            WebApiConfig.Register(httpConfiguration);

            var httpRouteData = new HttpRouteData(
                httpConfiguration.Routes[routeName],
                routeValueDict
            );

            self.Request = new HttpRequestMessage(httpMethod, "http://localhost/api/" + uri)
            {
                Properties =
                {
                    {HttpPropertyKeys.HttpConfigurationKey, httpConfiguration},
                    {HttpPropertyKeys.HttpRouteDataKey, httpRouteData}
                }
            };
        }
    }

}
