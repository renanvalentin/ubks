﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Hosting;
using NUnit.Framework;
using UBKS.Contracts;
using UBKS.Models;
using UBKS.Tests.SampleData;
using UBKS.Tests.Unity.Helper;
using UBKS.WebApi.Controllers;
using Moq;
using UBKS.WebApi;
using System.Web.Http.Routing;
using Newtonsoft.Json;
using System.Net;

namespace UBKS.Tests.Unity.Api
{
    [TestFixture]
    public class UnitControllerTests
    {
        private Mock<IUBKSUnitOfWork> _unitOfWork;

        [SetUp]
        public void Setup()
        {
            _unitOfWork = new Mock<IUBKSUnitOfWork>();
        }

        [Test]
        public void Post_ValidUnit_CreatedStatusCode()
        {
            _unitOfWork.Setup(x => x.Unit.Add(It.IsAny<Unit>()));

            var httpConfiguration = new HttpConfiguration();
            WebApiConfig.Register(httpConfiguration);

            var unitController = new UnitController(_unitOfWork.Object);
            unitController.MockRequest(WebApiConfig.ControllerOnly, "unit", HttpMethod.Post, new { controller = "unit" });

            var validUnit = UnidadeData.RetornaUnidades().FirstOrDefault();
            var response = unitController.Post(validUnit);
            var newStudent = JsonConvert.DeserializeObject<Student>(response.Content.ReadAsStringAsync().Result);

            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
            Assert.AreEqual(string.Format("http://localhost/api/unit/{0}", newStudent.Id), response.Headers.Location.ToString());
        }

        [Test]
        public void Post_StudentUnder18YearsWithoutResponsible_BadRequest()
        {
            _unitOfWork.Setup(x => x.Unit.Add(It.IsAny<Unit>()));

            var httpConfiguration = new HttpConfiguration();
            WebApiConfig.Register(httpConfiguration);

            var unitController = new UnitController(_unitOfWork.Object);
            unitController.MockRequest(WebApiConfig.ControllerOnly, "unit", HttpMethod.Post, new { controller = "unit" });

            var validUnit = UnidadeData.RetornaUnidades().FirstOrDefault();
            unitController.ModelState.AddModelError("", "mock error message");

            var response = unitController.Post(validUnit);

            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
