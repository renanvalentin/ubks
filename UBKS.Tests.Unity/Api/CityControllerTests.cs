﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Hosting;
using System.Web.Http.Routing;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using UBKS.Contracts;
using UBKS.Data;
using UBKS.Data.Helpers;
using UBKS.Models;
using UBKS.Tests.SampleData;
using UBKS.WebApi;
using UBKS.WebApi.Controllers;
using System.Collections.Generic;

namespace UBKS.Tests.Unity.Api
{
    [TestFixture]
    public class CityControllerTests
    {
        private Mock<IUBKSUnitOfWork> _unitOfWork;

        [SetUp]
        public void Setup()
        {
            _unitOfWork = new Mock<IUBKSUnitOfWork>();
        }

        [Test]
        public void Get_All_ReturnAllCities()
        {
            var fakeCities = CidadeData.RetornaCidades();
            _unitOfWork.Setup(x => x.City.GetAll()).Returns(fakeCities);

            var cityController = new CityController(_unitOfWork.Object)
            {
                Request = new HttpRequestMessage()
                {
                    Properties =
                    {
                        {HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration()}
                    }
                }
            };

            var cities = cityController.Get();

            Assert.IsNotNull(cities, "Result is null");
            Assert.IsInstanceOf(typeof(IEnumerable<City>), cities, "Wrong Model");
            Assert.Greater(cities.Count(), 0, "Got wrong number of cities");
        }

        [Test]
        public void Post_City_Returns_CreatedStatusCode()
        {
            _unitOfWork.Setup(x => x.City.Add(It.IsAny<City>()));

            var httpConfiguration = new HttpConfiguration();
            WebApiConfig.Register(httpConfiguration);

            var httpRouteData = new HttpRouteData(
                httpConfiguration.Routes[WebApiConfig.ControllerOnly],
                new HttpRouteValueDictionary { { "controller", "city" } }
            );

            var cityController = new CityController(_unitOfWork.Object)
            {
                Request = new HttpRequestMessage(HttpMethod.Post, "http://localhost/api/city")
                {
                    Properties =
                    {
                        { HttpPropertyKeys.HttpConfigurationKey, httpConfiguration },
                        { HttpPropertyKeys.HttpRouteDataKey, httpRouteData }
                    }
                }
            };

            var validCity = CidadeData.RetornaCidades().FirstOrDefault();
            var response = cityController.Post(validCity);
            var newCity = JsonConvert.DeserializeObject<City>(response.Content.ReadAsStringAsync().Result);

            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
            Assert.AreEqual(string.Format("http://localhost/api/city/{0}", newCity.Id), response.Headers.Location.ToString());
        }

        [Test]
        public void Post_EmptyCity_Returns_BadRequestStatusCode()
        {
            _unitOfWork.Setup(x => x.City.Add(It.IsAny<City>()));

            var httpConfiguration = new HttpConfiguration();
            WebApiConfig.Register(httpConfiguration);

            var httpRouteData = new HttpRouteData(
                httpConfiguration.Routes[WebApiConfig.ControllerOnly],
                new HttpRouteValueDictionary { { "controller", "city" } }
            );

            var controller = new CityController(_unitOfWork.Object)
            {
                Request = new HttpRequestMessage(HttpMethod.Post, "http://localhost/api/cidade")
                {
                    Properties =
                    {
                        { HttpPropertyKeys.HttpConfigurationKey, httpConfiguration },
                        { HttpPropertyKeys.HttpRouteDataKey, httpRouteData }
                    }
                }
            };

            var city = new City { Id = 0, Name = "" };

            controller.ModelState.AddModelError("", "mock error message");

            var response = controller.Post(city);

            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
