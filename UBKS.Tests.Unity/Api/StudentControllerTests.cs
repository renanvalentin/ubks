﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Hosting;
using NUnit.Framework;
using UBKS.Contracts;
using UBKS.Models;
using UBKS.Tests.SampleData;
using UBKS.Tests.Unity.Helper;
using UBKS.WebApi.Controllers;
using Moq;
using UBKS.WebApi;
using System.Web.Http.Routing;
using Newtonsoft.Json;
using System.Net;
using UBKS.WebApi.Models;

namespace UBKS.Tests.Unity.Api
{
    [TestFixture]
    public class StudentControllerTests
    {
        private Mock<IUBKSUnitOfWork> _unitOfWork;

        [SetUp]
        public void Setup()
        {
            _unitOfWork = new Mock<IUBKSUnitOfWork>();
        }

        [Test]
        public void Post_StudentUnder18YearsResponsible_CreatedStatusCode()
        {
            _unitOfWork.Setup(x => x.Student.Add(It.IsAny<Student>()));

            var httpConfiguration = new HttpConfiguration();
            WebApiConfig.Register(httpConfiguration);

            var studentController = new StudentController(_unitOfWork.Object);
            studentController.MockRequest(WebApiConfig.ControllerOnly, "student", HttpMethod.Post, new { controller = "student" });

            var validStudent = AlunoData.RetornaAlunos().FirstOrDefault();

            var studentDto = new StudentDto(validStudent);

            var response = studentController.Post(studentDto);
            var newStudent = JsonConvert.DeserializeObject<Student>(response.Content.ReadAsStringAsync().Result);

            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
            Assert.AreEqual(string.Format("http://localhost/api/student/{0}", newStudent.Id), response.Headers.Location.ToString());
        }

        [Test]
        public void Post_StudentUnder18YearsWithoutResponsible_BadRequest()
        {
            _unitOfWork.Setup(x => x.Student.Add(It.IsAny<Student>()));

            var httpConfiguration = new HttpConfiguration();
            WebApiConfig.Register(httpConfiguration);

            var studentController = new StudentController(_unitOfWork.Object);
            studentController.MockRequest(WebApiConfig.ControllerOnly, "student", HttpMethod.Post, new { controller = "student" });

            var validStudent = AlunoData.RetornaAlunos().FirstOrDefault();
            validStudent.Responsible = null;

            var studentDto = new StudentDto(validStudent);

            studentController.ModelState.AddModelError("", "mock error message");

            var response = studentController.Post(studentDto);

            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
