﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBKS.Models;

namespace UBKS.Tests.SampleData
{
    public static class ContatoData
    {
        public static List<UnitContact> RetornaContatosUnidades()
        {
            return new List<UnitContact> {
                new UnitContact {
                    Name = "Maria Alessandra",
                    CellPhone = "17 9181 3643",
                    Email = "maria.alessandra@outlook.com",
                    Phone = "17 3242 8181"                    
                },

                new UnitContact {
                    Name = "José Alceu",
                    CellPhone = "17 4123 1444",
                    Email = "jose.alceu@outlook.com",
                    Phone = "17 3242 8282"                    
                }
            };
        }

        public static List<StudentContact> RetornaContatosAlunos()
        {
            return new List<StudentContact> {
                new StudentContact {
                    Name = "Maria Alessandra",
                    CellPhone = "17 9181 3643",
                    Email = "maria.alessandra@outlook.com",
                    Phone = "17 3242 8181"                    
                },

                new StudentContact {
                    Name = "José Alceu",
                    CellPhone = "17 4123 1444",
                    Email = "jose.alceu@outlook.com",
                    Phone = "17 3242 8282"                    
                }
            };
        }
    }
}
