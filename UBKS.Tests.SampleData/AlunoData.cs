﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UBKS.Models;
using System.Collections.Generic;

namespace UBKS.Tests.SampleData
{
    public static class AlunoData
    {
        public static List<Student> RetornaAlunos()
        {
            return new List<Student> {
                new Student {
                    FirstName = "Renan Valentin",
                    LastName = "Ferreira",
                    Cpf = "397.008.268-48",
                    Rg = "48.446.040-7",
                    CodeTKFI = Guid.NewGuid().ToString(),
                    BirthDate = new DateTime(1991, 07, 18)
                }
            };
        }
    }
}
