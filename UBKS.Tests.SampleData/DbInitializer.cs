﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBKS.Data;
using UBKS.Models;

namespace UBKS.Tests.SampleData
{
    public class DbInitializer : DropCreateDatabaseAlways<UBKSDbContext>
    {
        protected override void Seed(UBKSDbContext context)
        {
            CreateOrganization(context);

            context.SaveChanges();

            CreateStudent(context);

            context.SaveChanges();
        }

        private void CreateOrganization(UBKSDbContext context)
        {
            var organization = new Organization
            {
                Name = "União Brasil Karate-Do Saito-Ha Shito-Ryu"
            };

            var unit = new Unit
            {
                Address = new Address
                {
                    City = "Mirasosl",
                    Neighborhood = "Centro",
                    Street = "Rua Dom Pedro II",
                    Number = "2455",
                    ZipCode = "15130-000",
                    State = "SP"
                },
                City = new City
                {
                    Name = "Mirassol",
                    State = new State
                    {
                        Initials = "SP",
                        Name = "São Paulo"
                    }
                },
                Name = "UBKS Mirassol"
            };

            organization.Units = new List<Unit>();
            organization.Units.Add(unit);

            context.Organizations.Add(organization);
        }

        private void CreateStudent(UBKSDbContext context)
        {
            var student = new Student
            {
                Unit = context.Units.First(),
                FirstName = "Renan",
                LastName = "Ferreira",
                Cpf = "397",
                Rg = "48",
                BirthDate = new DateTime(1991, 07, 18),
                CodeTKFI = "123",
                Responsible = new Responsible
                {
                    BirthDate = DateTime.Now,
                    Cpf = "397.008",
                    FirstName = "Aeols",
                    LastName = "Ferror",
                    Rg = "12.123"
                },
                Contacts = ContatoData.RetornaContatosAlunos(),

                Address = new Address
                {
                    City = "Mirasosl",
                    Neighborhood = "Centro",
                    Street = "Rua Dom Pedro II",
                    Number = "2455",
                    ZipCode = "15130-000",
                    State = "SP"
                }
            };

            student.Educations = new List<Education>();
            student.Educations.Add(
                    new Education
                    {
                        BeginDate = DateTime.Now,
                        Course = "Ensino Medio",
                        Grade = "3 colegial",
                        Institution = "São Paulo",
                        Period = "Manha",
                        EndDate = DateTime.Now.AddDays(5)
                    });

            context.Students.Add(student);
        }
    }
}
