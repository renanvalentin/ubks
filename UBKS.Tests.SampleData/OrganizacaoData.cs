﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UBKS.Models;
using System.Collections.Generic;

namespace UBKS.Tests.SampleData
{
    public static class OrganizacaoData
    {
        public static List<Organization> RetornaOrganizacoes()
        {
            return new List<Organization> {
                new Organization {
                   Name = "UBKS"                   
                }
            };
        }
    }
}
