﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBKS.Models;

namespace UBKS.Tests.SampleData
{
    public static class EnderecoData
    {
        public static List<Address> RetornaEnderecos()
        {
            return new List<Address> {
                new Address {
                    Neighborhood = "Portal",
                    ZipCode = "15130-000",
                    Street = "Av. Tarraf",
                    Number = "3359",
                    City= "Mirassol",
                    State = "São Paulo"
                },

                new Address {
                    Neighborhood = "Regissol",
                    ZipCode = "15130-000",
                    Street = "Rua 37",
                    Number = "2429",
                    City= "Mirassol",
                    State = "São Paulo"
                }
            };
        }
    }
}
