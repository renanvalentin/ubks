﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBKS.Models;

namespace UBKS.Tests.SampleData
{
    public static class EscolaridadeData
    {
        public static List<Education> RetornaEscolaridades()
        {
            return new List<Education> {
                new Education {
                    Course = "Ensino Medio",
                    Institution = "TUFI Madi",
                    Period = "Manha",
                    Grade = "3 ano"
                }
            };
        }
    }
}
