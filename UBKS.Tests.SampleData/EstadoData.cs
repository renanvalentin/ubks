﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBKS.Models;

namespace UBKS.Tests.SampleData
{
    public static class EstadoData
    {
        public static List<State> RetornaEstados()
        {
            return new List<State> {
                new State {
                  Name = "São Paulo",
                  Initials = "SP"
                }
            };
        }
    }
}
