﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBKS.Models;

namespace UBKS.Tests.SampleData
{
    public static class CidadeData
    {
        public static List<City> RetornaCidades()
        {
            return new List<City> {
                new City {
                    Name = "Mirassol",
                    State = EstadoData.RetornaEstados().First()                    
                }
            };
        }
    }
}
