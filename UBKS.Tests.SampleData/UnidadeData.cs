﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBKS.Models;

namespace UBKS.Tests.SampleData
{
    public static class UnidadeData
    {
        public static List<Unit> RetornaUnidades()
        {
            return new List<Unit> {
                new Unit {
                    Name = "UBKS Mirassol",
                    Address = EnderecoData.RetornaEnderecos()[1],
                    Contacts = ContatoData.RetornaContatosUnidades(),
                    City = CidadeData.RetornaCidades().First(),
                    Organization = OrganizacaoData.RetornaOrganizacoes().First()
                }
            };
        }
    }
}
