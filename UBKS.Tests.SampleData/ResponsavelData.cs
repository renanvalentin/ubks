﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UBKS.Models;
using System.Collections.Generic;

namespace UBKS.Tests.SampleData
{
    public static class ResponsavelData
    {
        public static List<Responsible> RetornaResponsaveis()
        {
            return new List<Responsible> {
                new Responsible {
                   FirstName = "Maria Antonia",
                   LastName = "Valentin",
                   Rg = "22.234.423-3",
                   Cpf = "321.312.523-42"
                }
            };
        }
    }
}
