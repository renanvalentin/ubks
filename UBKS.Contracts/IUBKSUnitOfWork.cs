﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBKS.Models;

namespace UBKS.Contracts
{
    public interface IUBKSUnitOfWork
    {
        // Save pending changes to the data store.
        void Commit();

        // Repositories
        IRepository<State> State { get; }
        IRepository<City> City { get; }
        IRepository<Student> Student { get; }
        IRepository<Education> Education { get; }
        IRepository<Responsible> Responsible { get; }
        IRepository<Graduation> Graduation { get; }
        IRepository<Payment> Payment { get; }
        IRepository<StudentGraduation> StudentGraduation { get; }
        IRepository<StudentContact> StudentContact { get; }
        IRepository<Unit> Unit { get; }

        IRepository<User> User { get; }

    }
}
